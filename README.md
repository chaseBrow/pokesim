# pokesim

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## REST API Table
| Request Type   | Resource Endpoint        | Body Example  | Expected Response  | Inner Working of Handler|
| :------------: | :----------------------- | :------------ | :----------------- | ----------------------- |
|   GET          | /party/                  | No Body       | String formatted json of all party members and their corresponding attributes| GET_INDEX uses get_party_members() from pdb |
|   GET          | /party/:slot_ID          | No Body       | String formatted json of  a party member in specific slot number from 1-6 and its corresponding attributes| GET_KEY uses get_party_member() from pdb|
|   PUT          | /party/:slot_ID          | \{“ability_ID” : “1”,”move1_ID” : “4”\} | \{“result”:”success”\} if it works| PUT_KEY checks body for changed properties, applies changes to member in slot specified using set_party_member_ methods from pdb for each one|
|   POST         | /party/:slot_ID          | \{"slot_ID" = "1", "dex_ID" = "1", "form_name" = "Bulbasaur", "gender" = "Male", "nature_ID" = "2", "ability_ID" = "46", "item_ID" = "3", "move1_ID" = "3", "move2_ID" = "224", "move3_ID" = "7", "move4_ID" = "253"\} | \{“result”:”success”\} if it works| POST_INDEX Uses set_party member(var) from pdb to take information and create a party member in the database|
|   DELETE       | /party/                  | \{\}| \{“result”:”success”\} if it works| DELETE_INDEX Deletes all party members using delete_party() from pdb
|   DELETE       | /party/:slot_ID          | \{\}| \{“result”:”success”\} if it works| DELETE_KEY Uses delete_party_member(slot_id) to delete specified party member
|   GET          | /pokemon/                | No Body| String formatted json of all pokemon in pokedex, shows national dex number, the form name, and other information for each pokemon| GET_INDEX Uses get_pokemon_all() from pdb for keys and then uses get_pokemon_one(dex_ID) |
|   GET          | /pokemon/:dex_ID         | No Body| String formatted json of pokedex information for one pokemon in the dex, shows national dex number, the form name, and other information| GET_KEY Uses get_pokemon_one(dex_ID) from pdb|
|   GET          | /abilities/              | No Body| String formatted json of all ability names and descriptions| GET_INDEX Uses get_abilities() from pdb for keys and then uses get_ability(ability_ID)|
|   GET          | /abilities/:ability_ID   | No Body| String formatted json containing the ability name and the corresponding description for one ability| GET_KEY Uses get_ability(ability_ID) from pd|
|   GET          | /items/                  | No Body| String formatted json of all items and item descriptions| GET_INDEX Uses get_items() from pdb for keys and then uses get_item(item_ID)|
|   GET          | /items/:item_ID          | No Body| String formatted json containing item name and corresponding item description| GET_KEY Uses get_item(item_ID)
|   GET          | /moves/                  | No Body| String formatted json of all moves, includes name, description, type, category, power, accuracy, pp, priority| GET_INDEX Uses get_moves() from pdb for keys and then uses get_move(move_ID)|
|   GET          | /moves/:move_ID          | No Body| String formatted json of specified move, includes name, description, type, category, power, accuracy, pp, priority| GET_KEY uses get_move(move_ID)|
|   GET          | /natures/                | No Body| String formatted json of all natures and their corresponding multipliers for each stat category| GET_INDEX Uses get_natures() from pdb for keys and then uses get_nature(nature_ID)|
|   GET          | /natures/:nature_ID      | No Body| String formatted json of one nature and its corresponding multipliers for each stat category| GET_KEY uses get_nature(nature_ID)|
|   GET          | /types/                  | No Body| String formatted json of all types and their corresponding multipliers for each type on defense| GET_INDEX Uses get_typecharts() from pdb for keys and then uses get_typechart(type_ID)|
|   GET          | /types/:type_ID          | No Body| String formatted json of one types and its corresponding multipliers for each type on defense| GET_KEY uses get_typechart(type_ID)|
|   PUT          | /reset/                  | No Body| \{“result”:”success”\} if it works| PUT_INDEX Reinitializes the saved members list and the party members list|
