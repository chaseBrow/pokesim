import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    curPokemon: null,
    parties: [
      { //party object
        name: "Team 1",
        isLightScreen: false,
        isReflect: false,
        isTailwind: false,
        isFriendGuard: false,
        pokemon: [
          { //pokemon obj
            slotIndex: 0,
            id: 598,
            name: "Ferrothorn",
            type1: "Grass",
            type2: "Steel",
            gender: true, //true = male, false = female, null
            nature: 23, //1-25
            ability: 130, // null or 1-167
            item: 45, //1 = no item
            moves: [
              { //move obj
                id: 451,
                name: "Power Whip",
                type: "Grass",
                category: "Physical", //Special, Physical, Non-Damaging
                power: 120,
                accuracy: 85,
                priority: 0,
                pp: 10,
                description: "No additional effect."
              },
              { //move obj
                id: 392,
                name: "Gyro Ball",
                type: "Steel",
                category: "Physical", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 100,
                priority: 0,
                pp: 5,
                description: "More power the slower the user than the target."
              },
              { //move obj
                id: 377,
                name: "Stealth Rock",
                type: "Rock",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 0,
                pp: 20,
                description: "Hurts foes on switch-in. Factors Rock weakness."
              },
              { //move obj
                id: 47,
                name: "Leech Seed",
                type: "Grass",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 90,
                priority: 0,
                pp: 10,
                description: "1/8 of target's HP is restored to user every turn."
              }
            ],
            battleProperties:
            {
              status: "Healthy", // Healthy, Poisoned, Badly Poisoned, Burned, Paralyzed, Asleep, Frozen, Confused, Charmed 
              maxHP: 0,
              currHP: 0,
              percentageHP: 0,//this.$store.state.currHP/this.maxHP,
              level: 100,
              weight: 110, //kg
              natureMod: {
                atk: 1,
                def: 1,
                spa: 1,
                spd: 1.1,
                spe: 0.9
              },
              baseStats:
              {
                hp: 74,
                atk: 94,
                def: 131,
                spa: 54,
                spd: 116,
                spe: 20
              },
              battleStats:
              {
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              statStages:
              {
                //-6 to 6 increments of 1 
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              battleState:
              {
                isGrounded: true,
                isSeeded: false,
                isForesighted: false,
                isSwitchingOut: false,
                isFainted: false
              }
            }
          },
          { //pokemon obj
            slotIndex: 1,
            id: 640,
            name: "Virizion",
            type1: "Grass",
            type2: "Fighting",
            gender: null, //true = male, false = female, null
            nature: 25, //1-25
            ability: 148, // null or 1-167
            item: 128, //1 = no item
            moves: [
              { //move obj
                id: 323,
                name: "Calm Mind",
                type: "Psychic",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 0,
                pp: 20,
                description: "Raises the user's Sp. Atk and Sp. Def by 1."
              },
              { //move obj
                id: 201,
                name: "Giga Drain",
                type: "Grass",
                category: "Special", //Special, Physical, Non-Damaging
                power: 75,
                accuracy: 100,
                priority: 0,
                pp: 10,
                description: "User recovers 50% of the damage dealt."
              },
              { //move obj
                id: 432,
                name: "Focus Blast",
                type: "Fighting",
                category: "Special", //Special, Physical, Non-Damaging
                power: 120,
                accuracy: 70,
                priority: 0,
                pp: 5,
                description: "10% chance to lower the target's Sp. Def by 1."
              },
              { //move obj
                id: 186,
                name: "Protect",
                type: "Normal",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 4,
                pp: 10,
                description: "Prevents moves from affecting the user this turn."
              }
            ],
            battleProperties:
            {
              status: "Healthy", // Healthy, Poisoned, Badly Poisoned, Burned, Paralyzed, Asleep, Frozen, Confused, Charmed 
              maxHP: 0,
              currHP: 0,
              percentageHP: 0,//this.$store.state.currHP/this.maxHP,
              level: 100,
              weight: 200, //kg
              natureMod: {
                atk: 0.9,
                def: 1,
                spa: 1,
                spd: 1,
                spe: 1.1
              },
              baseStats:
              {
                hp: 91,
                atk: 90,
                def: 72,
                spa: 90,
                spd: 129,
                spe: 108
              },
              battleStats:
              {
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              statStages:
              {
                //-6 to 6 increments of 1 
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              battleState:
              {
                isGrounded: true,
                isSeeded: false,
                isForesighted: false,
                isSwitchingOut: false,
                isFainted: false
              }
            }
          },
          { //pokemon obj
            slotIndex: 2,
            id: 385,
            name: "Jirachi",
            type1: "Psychic",
            type2: "Steel",
            gender: null, //true = male, false = female, null
            nature: 25, //1-25
            ability: 28, // null or 1-167
            item: 45, //1 = no item
            moves: [
              { //move obj
                id: 323,
                name: "Calm Mind",
                type: "Psychic",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 0,
                pp: 20,
                description: "Raises the user's Sp. Atk and Sp. Def by 1."
              },
              { //move obj
                id: 542,
                name: "Psyshock",
                type: "Psychic",
                category: "Special", //Special, Physical, Non-Damaging
                power: 80,
                accuracy: 100,
                priority: 0,
                pp: 10,
                description: "Damages target based on Defense, not Sp. Def."
              },
              { //move obj
                id: 88,
                name: "Thunder",
                type: "Electric",
                category: "Special", //Special, Physical, Non-Damaging
                power: 120,
                accuracy: 70,
                priority: 0,
                pp: 10,
                description: "30% chance to paralyze. Can't miss in rain."
              },
              { //move obj
                id: 1,
                name: "Grass Knot",
                type: "Grass",
                category: "Special", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 100,
                priority: 0,
                pp: 20,
                description: "More power the heavier the target."
              }
            ],
            battleProperties:
            {
              status: "Healthy", // Healthy, Poisoned, Badly Poisoned, Burned, Paralyzed, Asleep, Frozen, Confused, Charmed 
              maxHP: 0,
              currHP: 0,
              percentageHP: 0,//this.$store.state.currHP/this.maxHP,
              level: 100,
              weight: 1.1, //kg
              natureMod: {
                atk: 0.9,
                def: 1,
                spa: 1,
                spd: 1,
                spe: 1.1
              },
              baseStats:
              {
                hp: 100,
                atk: 100,
                def: 100,
                spa: 100,
                spd: 100,
                spe: 100
              },
              battleStats:
              {
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              statStages:
              {
                //-6 to 6 increments of 1 
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              battleState:
              {
                isGrounded: true,
                isSeeded: false,
                isForesighted: false,
                isSwitchingOut: false,
                isFainted: false
              }
            }
          },
          { //pokemon obj
            slotIndex: 3,
            id: 230,
            name: "Kingdra",
            type1: "Water",
            type2: "Dragon",
            gender: true, //true = male, false = female, null
            nature: 16, //1-25
            ability: 73, // null or 1-167
            item: 122, //1 = no item Choice Specs
            moves: [
              { //move obj
                id: 388,
                name: "Draco Meteor",
                type: "Dragon",
                category: "Special", //Special, Physical, Non-Damaging
                power: 140,
                accuracy: 90,
                priority: 0,
                pp: 5,
                description: "Lowers the user's Sp. Atk by 2."
              },
              { //move obj
                id: 112,
                name: "Hydro Pump",
                type: "Water",
                category: "Special", //Special, Physical, Non-Damaging
                power: 120,
                accuracy: 80,
                priority: 0,
                pp: 5,
                description: "No additional effect."
              },
              { //move obj
                id: 389,
                name: "Dragon Pulse",
                type: "Dragon",
                category: "Special", //Special, Physical, Non-Damaging
                power: 90,
                accuracy: 100,
                priority: 0,
                pp: 10,
                description: "No additional effect."
              },
              { //move obj
                id: 104,
                name: "Surf",
                type: "Water",
                category: "Special", //Special, Physical, Non-Damaging
                power: 95,
                accuracy: 100,
                priority: 0,
                pp: 15,
                description: "Hits adjacent Pokemon. Double damage on Dive."
              }
            ],
            battleProperties:
            {
              status: "Healthy", // Healthy, Poisoned, Badly Poisoned, Burned, Paralyzed, Asleep, Frozen, Confused, Charmed 
              maxHP: 0,
              currHP: 0,
              percentageHP: 0,//this.$store.state.currHP/this.maxHP,
              level: 100,
              weight: 152, //kg
              natureMod: {
                atk: 0.9,
                def: 1,
                spa: 1.1,
                spd: 1,
                spe: 1
              },
              baseStats:
              {
                hp: 45,
                atk: 49,
                def: 49,
                spa: 65,
                spd: 65,
                spe: 45
              },
              battleStats:
              {
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              statStages:
              {
                //-6 to 6 increments of 1 
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              battleState:
              {
                isGrounded: true,
                isSeeded: false,
                isForesighted: false,
                isSwitchingOut: false,
                isFainted: false
              }
            }
          },
          { //pokemon obj
            slotIndex: 4,
            id: 186,
            name: "Politoed",
            type1: "Water",
            type2: null,
            gender: true, //true = male, false = female, null
            nature: 16, //1-25
            ability: 49, // null or 1-167
            item: 45, //1 = no item
            moves: [
              { //move obj
                id: 104,
                name: "Surf",
                type: "Water",
                category: "Special", //Special, Physical, Non-Damaging
                power: 95,
                accuracy: 100,
                priority: 0,
                pp: 15,
                description: "Hits adjacent Pokemon. Double damage on Dive."
              },
              { //move obj
                id: 113,
                name: "Ice Beam",
                type: "Ice",
                category: "Special", //Special, Physical, Non-Damaging
                power: 95,
                accuracy: 100,
                priority: 0,
                pp: 10,
                description: "10% chance to freeze the target."
              },
              { //move obj
                id: 432,
                name: "Focus Blast",
                type: "Fighting",
                category: "Special", //Special, Physical, Non-Damaging
                power: 120,
                accuracy: 70,
                priority: 0,
                pp: 5,
                description: "10% chance to lower the target's Sp. Def by 1."
              },
              { //move obj
                id: 58,
                name: "Psychic",
                type: "Psychic",
                category: "Special", //Special, Physical, Non-Damaging
                power: 90,
                accuracy: 100,
                priority: 0,
                pp: 10,
                description: "10% chance to lower the target's Sp. Def by 1."
              }
            ],
            battleProperties:
            {
              status: "Healthy", // Healthy, Poisoned, Badly Poisoned, Burned, Paralyzed, Asleep, Frozen, Confused, Charmed 
              maxHP: 0,
              currHP: 0,
              percentageHP: 0,//this.$store.state.currHP/this.maxHP,
              level: 100,
              weight: 34, //kg
              natureMod: {
                atk: 0.9,
                def: 1,
                spa: 1.1,
                spd: 1,
                spe: 1
              },
              baseStats:
              {
                hp: 90,
                atk: 75,
                def: 75,
                spa: 90,
                spd: 100,
                spe: 70
              },
              battleStats:
              {
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              statStages:
              {
                //-6 to 6 increments of 1 
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              battleState:
              {
                isGrounded: true,
                isSeeded: false,
                isForesighted: false,
                isSwitchingOut: false,
                isFainted: false
              }
            }
          },
          { //pokemon obj
            slotIndex: 5,
            id: 490,
            name: "Manaphy",
            type1: "Water",
            type2: null,
            gender: null, //true = male, false = female, null
            nature: 25, //1-25
            ability: 100, // null or 1-167
            item: 45, //1 = no item
            moves: [
              { //move obj
                id: 323,
                name: "Calm Mind",
                type: "Psychic",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 0,
                pp: 20,
                description: "Raises the user's Sp. Atk and Sp. Def by 1."
              },
              { //move obj
                id: 490,
                name: "Scald",
                type: "Water",
                category: "Special", //Special, Physical, Non-Damaging
                power: 80,
                accuracy: 100,
                priority: 0,
                pp: 15,
                description: "30% chance to burn the target."
              },
              { //move obj
                id: 113,
                name: "Ice Beam",
                type: "Ice",
                category: "Special", //Special, Physical, Non-Damaging
                power: 95,
                accuracy: 100,
                priority: 0,
                pp: 10,
                description: "10% chance to freeze the target."
              },
              { //move obj
                id: 65,
                name: "Rest",
                type: "Psychic",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 0,
                pp: 10,
                description: "User sleeps 2 turns and restores HP and status."
              }
            ],
            battleProperties:
            {
              status: "Healthy", // Healthy, Poisoned, Badly Poisoned, Burned, Paralyzed, Asleep, Frozen, Confused, Charmed 
              maxHP: 0,
              currHP: 0,
              percentageHP: 0,//this.$store.state.currHP/this.maxHP,
              level: 100,
              weight: 1.4, //kg
              natureMod: {
                atk: 0.9,
                def: 1,
                spa: 1,
                spd: 1,
                spe: 1.1
              },
              baseStats:
              {
                hp: 100,
                atk: 100,
                def: 100,
                spa: 100,
                spd: 100,
                spe: 100
              },
              battleStats:
              {
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              statStages:
              {
                //-6 to 6 increments of 1 
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              battleState:
              {
                isGrounded: true,
                isSeeded: false,
                isForesighted: false,
                isSwitchingOut: false,
                isFainted: false
              }
            }
          },
        ]
      },
      { //party object
        name: "Team 2",
        isLightScreen: false,
        isReflect: false,
        isTailwind: false,
        isFriendGuard: false,
        pokemon: [
          { //pokemon obj
            slotIndex: 0,
            id: 593,
            name: "Jellicent",
            type1: "Water",
            type2: "Ghost",
            gender: true, //true = male, false = female, null
            nature: 3, //1-25
            ability: 74, // null or 1-167
            item: 45, //1 = no item
            moves: [
              { //move obj
                id: 246,
                name: "Shadow Ball",
                type: "Ghost",
                category: "Special", //Special, Physical, Non-Damaging
                power: 80,
                accuracy: 100,
                priority: 0,
                pp: 15,
                description: "20% chance to lower the target's Sp. Def by 1."
              },
              { //move obj
                id: 305,
                name: "Wll-O-Wisp",
                type: "Fire",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 75,
                priority: 0,
                pp: 15,
                description: "Burns the target."
              },
              { //move obj
                id: 490,
                name: "Scald",
                type: "Water",
                category: "Special", //Special, Physical, Non-Damaging
                power: 80,
                accuracy: 100,
                priority: 0,
                pp: 15,
                description: "30% chance to burn the target."
              },
              { //move obj
                id: 63,
                name: "Recover",
                type: "Normal",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 0,
                pp: 10,
                description: "Heals the user by 50% of its max HP."
              }
            ],
            battleProperties:
            {
              status: "Healthy", // Healthy, Poisoned, Badly Poisoned, Burned, Paralyzed, Asleep, Frozen, Confused, Charmed 
              maxHP: 0,
              currHP: 0,
              percentageHP: 0,//this.$store.state.currHP/this.maxHP,
              level: 100,
              weight: 135, //kg
              natureMod: {
                atk: 0.9,
                def: 1.1,
                spa: 1,
                spd: 1,
                spe: 1
              },
              baseStats:
              {
                hp: 100,
                atk: 60,
                def: 70,
                spa: 85,
                spd: 105,
                spe: 60
              },
              battleStats:
              {
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              statStages:
              {
                //-6 to 6 increments of 1 
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              battleState:
              {
                isGrounded: true,
                isSeeded: false,
                isForesighted: false,
                isSwitchingOut: false,
                isFainted: false
              }
            }
          },
          { //pokemon obj
            slotIndex: 1,
            id: 205,
            name: "Forretress",
            type1: "Bug",
            type2: "Steel",
            gender: true, //true = male, false = female, null
            nature: 22, //1-25
            ability: 31, // null or 1-167
            item: 45, //1 = no item
            moves: [
              { //move obj
                id: 380,
                name: "Toxic Spikes",
                type: "Poison",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 0,
                pp: 20,
                description: "Poisons grounded foes on switch-in. Max 2 layers."
              },
              { //move obj
                id: 211,
                name: "Rapid Spin",
                type: "Normal",
                category: "Physical", //Special, Physical, Non-Damaging
                power: 20,
                accuracy: 100,
                priority: 0,
                pp: 40,
                description: "Frees user from hazards, binding, Leech Seed."
              },
              { //move obj
                id: 392,
                name: "Gyro Ball",
                type: "Steel",
                category: "Physical", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 100,
                priority: 0,
                pp: 5,
                description: "More power the slower the user than the target."
              },
              { //move obj
                id: 559,
                name: "Volt Switch",
                type: "Electric",
                category: "Special", //Special, Physical, Non-Damaging
                power: 70,
                accuracy: 100,
                priority: 0,
                pp: 20,
                description: "User switches out after damaging the target."
              }
            ],
            battleProperties:
            {
              status: "Healthy", // Healthy, Poisoned, Badly Poisoned, Burned, Paralyzed, Asleep, Frozen, Confused, Charmed 
              maxHP: 0,
              currHP: 0,
              percentageHP: 0,//this.$store.state.currHP/this.maxHP,
              level: 100,
              weight: 126, //kg
              natureMod: {
                atk: 1,
                def: 1.1,
                spa: 1,
                spd: 1,
                spe: 0.9
              },
              baseStats:
              {
                hp: 75,
                atk: 90,
                def: 140,
                spa: 60,
                spd: 60,
                spe: 40
              },
              battleStats:
              {
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              statStages:
              {
                //-6 to 6 increments of 1 
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              battleState:
              {
                isGrounded: true,
                isSeeded: false,
                isForesighted: false,
                isSwitchingOut: false,
                isFainted: false
              }
            }
          },
          { //pokemon obj
            slotIndex: 2,
            id: 485,
            name: "Heatran",
            type1: "Fire",
            type2: "Steel",
            gender: true, //true = male, false = female, null
            nature: 5, //1-25
            ability: 4, // null or 1-167
            item: 45, //1 = no item
            moves: [
              { //move obj
                id: 443,
                name: "Lava Plume",
                type: "Fire",
                category: "Special", //Special, Physical, Non-Damaging
                power: 80,
                accuracy: 100,
                priority: 0,
                pp: 15,
                description: "30% chance to burn adjacent Pokemon."
              },
              { //move obj
                id: 66,
                name: "Roar",
                type: "Normal",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 100,
                priority: -6,
                pp: 20,
                description: "Forces the target to switch to a random ally."
              },
              { //move obj
                id: 377,
                name: "Stealth Rock",
                type: "Rock",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 0,
                pp: 20,
                description: "Hurts foes on switch-in. Factors Rock weakness."
              },
              { //move obj
                id: 186,
                name: "Protect",
                type: "Normal",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 4,
                pp: 10,
                description: "Prevents moves from affecting the user this turn."
              }
            ],
            battleProperties:
            {
              status: "Healthy", // Healthy, Poisoned, Badly Poisoned, Burned, Paralyzed, Asleep, Frozen, Confused, Charmed 
              maxHP: 0,
              currHP: 0,
              percentageHP: 0,//this.$store.state.currHP/this.maxHP,
              level: 100,
              weight: 430, //kg
              natureMod: {
                atk: 0.9,
                def: 1,
                spa: 1,
                spd: 1.1,
                spe: 1
              },
              baseStats:
              {
                hp: 91,
                atk: 90,
                def: 106,
                spa: 130,
                spd: 106,
                spe: 77
              },
              battleStats:
              {
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              statStages:
              {
                //-6 to 6 increments of 1 
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              battleState:
              {
                isGrounded: true,
                isSeeded: false,
                isForesighted: false,
                isSwitchingOut: false,
                isFainted: false
              }
            }
          },
          { //pokemon obj
            slotIndex: 3,
            id: 640,
            name: "Virizion",
            type1: "Grass",
            type2: "Fighting",
            gender: null, //true = male, false = female, null
            nature: 12, //1-25
            ability: 148, // null or 1-167
            item: 45, //1 = no item
            moves: [
              { //move obj
                id: 123,
                name: "Swords Dance",
                type: "Normal",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 0,
                pp: 30,
                description: "Raises the user's Attack by 2."
              },
              { //move obj
                id: 421,
                name: "Close Combat",
                type: "Fighting",
                category: "Physical", //Special, Physical, Non-Damaging
                power: 120,
                accuracy: 100,
                priority: 0,
                pp: 5,
                description: "Lowers the user's Defense and Sp. Def by 1."
              },
              { //move obj
                id: 260,
                name: "Leaf Blade",
                type: "Grass",
                category: "Physical", //Special, Physical, Non-Damaging
                power: 90,
                accuracy: 100,
                priority: 0,
                pp: 15,
                description: "High critical hit ratio."
              },
              { //move obj
                id: 463,
                name: "Stone Edge",
                type: "Rock",
                category: "Physical", //Special, Physical, Non-Damaging
                power: 100,
                accuracy: 80,
                priority: 0,
                pp: 5,
                description: "High critical hit ratio."
              }
            ],
            battleProperties:
            {
              status: "Healthy", // Healthy, Poisoned, Badly Poisoned, Burned, Paralyzed, Asleep, Frozen, Confused, Charmed 
              maxHP: 0,
              currHP: 0,
              percentageHP: 0,//this.$store.state.currHP/this.maxHP,
              level: 100,
              weight: 6.9, //kg
              natureMod: {
                atk: 1,
                def: 1,
                spa: 0.9,
                spd: 1,
                spe: 1.1
              },
              baseStats:
              {
                hp: 91,
                atk: 90,
                def: 72,
                spa: 90,
                spd: 129,
                spe: 108
              },
              battleStats:
              {
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              statStages:
              {
                //-6 to 6 increments of 1 
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              battleState:
              {
                isGrounded: true,
                isSeeded: false,
                isForesighted: false,
                isSwitchingOut: false,
                isFainted: false
              }
            }
          },
          { //pokemon obj
            slotIndex: 4,
            id: 151,
            name: "Mew",
            type1: "Psychic",
            type2: null,
            gender: null, //true = male, false = female, null
            nature: 25, //1-25 Timid
            ability: 33, // null or 1-167
            item: 45, //1 = no item
            moves: [
              { //move obj
                id: 445,
                name: "Nasty Plot",
                type: "Dark",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 0,
                pp: 20,
                description: "Raises the user's Sp. Atk by 2."
              },
              { //move obj
                id: 58,
                name: "Psychic",
                type: "Psychic",
                category: "Special", //Special, Physical, Non-Damaging
                power: 90,
                accuracy: 100,
                priority: 0,
                pp: 10,
                description: "10% chance to lower the target's Sp. Def by 1."
              },
              { //move obj
                id: 386,
                name: "Aura Sphere",
                type: "Fighting",
                category: "Special", //Special, Physical, Non-Damaging
                power: 90,
                accuracy: 0,
                priority: 0,
                pp: 20,
                description: "This move does not check accuracy."
              },
              { //move obj
                id: 110,
                name: "Flamethrower",
                type: "Fire",
                category: "Special", //Special, Physical, Non-Damaging
                power: 95,
                accuracy: 100,
                priority: 0,
                pp: 15,
                description: "10% chance to burn the target."
              }
            ],
            battleProperties:
            {
              status: "Healthy", // Healthy, Poisoned, Badly Poisoned, Burned, Paralyzed, Asleep, Frozen, Confused, Charmed 
              maxHP: 0,
              currHP: 0,
              percentageHP: 0,//this.$store.state.currHP/this.maxHP,
              level: 100,
              weight: 4, //kg
              natureMod: {
                atk: 0.9,
                def: 1,
                spa: 1,
                spd: 1,
                spe: 1.1
              },
              baseStats:
              {
                hp: 100,
                atk: 100,
                def: 100,
                spa: 100,
                spd: 100,
                spe: 100
              },
              battleStats:
              {
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              statStages:
              {
                //-6 to 6 increments of 1 
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              battleState:
              {
                isGrounded: true,
                isSeeded: false,
                isForesighted: false,
                isSwitchingOut: false,
                isFainted: false
              }
            }
          },
          { //pokemon obj
            slotIndex: 5,
            id: 149,
            name: "Dragonite",
            type1: "Dragon",
            type2: "Flying",
            gender: true, //true = male, false = female, null
            nature: 12, //1-25
            ability: 152, // null or 1-167
            item: 45, //1 = no item
            moves: [
              { //move obj
                id: 327,
                name: "Dragon Dance",
                type: "Dragon",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 0,
                pp: 20,
                description: "Raises the user's Attack and Speed by 1."
              },
              { //move obj
                id: 326,
                name: "Dragon Claw",
                type: "Dragon",
                category: "Physical", //Special, Physical, Non-Damaging
                power: 80,
                accuracy: 100,
                priority: 0,
                pp: 15,
                description: "No additional effect."
              },
              { //move obj
                id: 83,
                name: "Substitute",
                type: "Normal",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 0,
                pp: 10,
                description: "User takes 1/4 its max HP to put in a substitute."
              },
              { //move obj
                id: 456,
                name: "Roost",
                type: "Flying",
                category: "Non-Damaging", //Special, Physical, Non-Damaging
                power: 0,
                accuracy: 0,
                priority: 0,
                pp: 10,
                description: "Heals 50% HP. Flying-type removed 'til turn ends."
              }
            ],
            battleProperties:
            {
              status: "Healthy", // Healthy, Poisoned, Badly Poisoned, Burned, Paralyzed, Asleep, Frozen, Confused, Charmed 
              maxHP: 0,
              currHP: 0,
              percentageHP: 0,//this.$store.state.currHP/this.maxHP,
              level: 100,
              weight: 210, //kg
              natureMod: {
                atk: 1,
                def: 1,
                spa: 0.9,
                spd: 1,
                spe: 1.1
              },
              baseStats:
              {
                hp: 91,
                atk: 134,
                def: 95,
                spa: 100,
                spd: 100,
                spe: 80
              },
              battleStats:
              {
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              statStages:
              {
                //-6 to 6 increments of 1 
                atk: 0,
                def: 0,
                spa: 0,
                spd: 0,
                spe: 0
              },
              battleState:
              {
                isGrounded: false,
                isSeeded: false,
                isForesighted: false,
                isSwitchingOut: false,
                isFainted: false
              }
            }
          },
        ]
      }
    ]
  },
  mutations: {
    //this.$store.commit("loadBattle", params);
    //these are calls which will be made to update the state
    loadBattle: function(state) {
      state.parties.forEach((party) => {
        party.pokemon.forEach((poke) => {
          poke.battleProperties.battleStats.atk = Math.floor((0.01 *(2 * poke.battleProperties.baseStats.atk * poke.battleProperties.level) + 5) * poke.battleProperties.natureMod.atk);
          poke.battleProperties.battleStats.def = Math.floor((0.01 *(2 * poke.battleProperties.baseStats.def * poke.battleProperties.level) + 5) * poke.battleProperties.natureMod.def);
          poke.battleProperties.battleStats.spa = Math.floor((0.01 *(2 * poke.battleProperties.baseStats.spa * poke.battleProperties.level) + 5) * poke.battleProperties.natureMod.spa);
          poke.battleProperties.battleStats.spd = Math.floor((0.01 *(2 * poke.battleProperties.baseStats.spd * poke.battleProperties.level) + 5) * poke.battleProperties.natureMod.spd);
          poke.battleProperties.battleStats.spe = Math.floor((0.01 *(2 * poke.battleProperties.baseStats.spe * poke.battleProperties.level) + 5) * poke.battleProperties.natureMod.spe);
          poke.battleProperties.maxHP = Math.floor((0.01 *(2 * poke.battleProperties.baseStats.hp * poke.battleProperties.level) + 5) + poke.battleProperties.level + 10);
          poke.battleProperties.currHP = poke.battleProperties.maxHP;
          poke.battleProperties.percentageHP = poke.battleProperties.currHP/poke.battleProperties.maxHP
        })
      })
      localStorage.parties = JSON.stringify(state.parties);
    },
    // addPokemon: function(state) {

    // },
    // addMove: function(state) {

    // },
    setCur: function(state, poke, party) {
      console.log(poke);
      state.curParty = party;
      state.curPokemon = poke;
    },
    addParty: function(state) {
      console.log(state)
    }
  },
  actions: {
    //this is where define calls to the api
    //to make async calls to call a mutation and update state
    addPokemon: function() {

    },
    addMove: function() {

    },
    addParty: function() {

    }
  },
  getters: {
    //when we need to get a pokemon... but not all of their data...
  }
})
