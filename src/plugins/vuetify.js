import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import colors from "vuetify/lib/util/colors";

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
		light: true,
		dark: false,

		themes: {
			light: {
				pokeR: "#FF0000", //
				pokeY: "#FFDE00", //
				pokeB: "#3B4CCA",
                Normal: "#A8A77A",
				Fire: "#EE8130",
				Water: "#6390F0",
				Electric: "#F7D02C",
				Grass: "#7AC74C",
				Ice: "#96D9D6",
				Fighting: "#C22E28",
				Poison: "#A33EA1",
				Ground: "#E2BF65",
				Flying: "#A98FF3",
				Psychic: "#F95587",
				Bug: "#A6B91A",
				Rock: "#B6A136",
				Ghost: "#735797",
				Dragon: "#6F35FC",
				Dark: "#705746",
				Steel: "#B7B7CE",
				info: colors.grey,
				success: colors.green,
				error: colors.red,
                background: colors.grey.lighten1
			},
			dark: {
				pokeR: "#FF0000", //
				pokeY: "#FFDE00", //
				pokeB: "#3B4CCA",
				info: colors.grey,
				success: colors.green,
				error: colors.red,
                background: colors.grey.lighten1
			}
		},
		iconfont: "md",
		icons: {
			iconfont: "md"
		}
    }
});
