import re, json
from pokemon_library import _pokemon_database

class PokemonController(object):

    def __init__(self, pdb=None):
        if pdb is None:
            self.pdb = _pokemon_database()
        else:
            self.pdb = pdb
        
        self.pdb.load_pokemon('pokemonandmoves.csv')

    def GET_KEY(self, dex_ID):
        output = {'result':'success'}
        dex_ID = int(dex_ID)

        try:
            pokemon = self.pdb.get_pokemon_one(dex_ID)
            if pokemon is not None:
                for form_name in pokemon.keys():
                    output['pokemon'] = pokemon[form_name]
            else:
                output['result'] = 'error'
                output['message'] = 'pokemon not found'
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
    
    def GET_INDEX(self):
        output = {'result':'success'}
        output['pokemon_list'] = []

        try:
            for dex_ID in self.pdb.get_pokemon_all():
                dpokemon = dict()
                pokemon = self.pdb.get_pokemon_one(dex_ID)
                if pokemon is not None:
                    for form_name in pokemon.keys():
                        dpokemon = pokemon[form_name]
                        output['pokemon_list'].append(dpokemon)
                else:
                    output['result'] = 'error'
                    output['message'] = 'pokemon not found'
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        
        return json.dumps(output)    