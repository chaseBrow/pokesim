import re, json
from pokemon_library import _pokemon_database

class TypeController(object):

    def __init__(self, pdb=None):
        if pdb is None:
            self.pdb = _pokemon_database()
        else:
            self.pdb = pdb
        
        self.pdb.load_typechart('types.csv')
    
    def GET_KEY(self, type_ID):
        output = {'result':'success'}
        type_ID = int(type_ID)

        try:
            typechart = self.pdb.get_typechart(type_ID)
            if typechart is not None:
                output['type object'] = typechart
            else:
                output['result'] = 'error'
                output['message'] = 'typechart not found'
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
    
    def GET_INDEX(self):
        output = {'result':'success'}
        output['typecharts'] = []

        try:
            for type_ID in self.pdb.get_typecharts():
                typechart = self.pdb.get_typechart(type_ID)
                output['typecharts'].append(typechart)
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        
        return json.dumps(output)