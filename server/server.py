from flask import Flask
from partyController import PartyController
from pokemonController import PokemonController
from abilitiesController import AbilitiesController
from itemController import ItemController
from movesController import MovesController
from naturesController import NaturesController
from typeController import TypeController
from resetController import ResetController
from pokemon_library import _pokemon_database

def start_service():

    pdb = _pokemon_database()

    partyController = PartyController(pdb=pdb)
    pokemonController = PokemonController(pdb=pdb)
    abilitiesController = AbilitiesController(pdb=pdb)
    itemController = ItemController(pdb=pdb)
    movesController = MovesController(pdb=pdb)
    naturesController = NaturesController(pdb=pdb)
    typeController = TypeController(pdb=pdb)
    resetController = ResetController(pdb=pdb)

    dispatcher.connect('party_get', '/party/:slot_ID', controller=partyController, action = 'GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('party_put', '/party/:slot_ID', controller=partyController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    dispatcher.connect('party_post', '/party/:slot_ID', controller=partyController, action = 'POST_KEY', conditions=dict(method=['POST']))
    dispatcher.connect('party_delete', '/party/:slot_ID', controller=partyController, action = 'DELETE_KEY', conditions=dict(method=['DELETE']))
    dispatcher.connect('party_index_get', '/party/', controller=partyController, action = 'GET_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('party_index_delete', '/party/', controller=partyController, action = 'DELETE_INDEX', conditions=dict(method=['DELETE']))
	
    dispatcher.connect('pokemon_get', '/pokemon/:dex_ID', controller=pokemonController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('pokemon_get_index', '/pokemon/', controller=pokemonController, action='GET_INDEX', conditions=dict(method=['GET']))

    dispatcher.connect('abilities_get', '/abilities/:ability_ID', controller=abilitiesController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('abilities_get_index', '/abilities/', controller=abilitiesController, action='GET_INDEX', conditions=dict(method=['GET']))

    dispatcher.connect('item_get', '/items/:item_ID', controller=itemController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('item_get_index', '/items/', controller=itemController, action='GET_INDEX', conditions=dict(method=['GET']))
    
    dispatcher.connect('moves_get', '/moves/:move_ID', controller=movesController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('moves_get_index', '/moves/', controller=movesController, action='GET_INDEX', conditions=dict(method=['GET']))

    dispatcher.connect('natures_get', '/natures/:nature_ID', controller=naturesController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('natures_get_index', '/natures/', controller=naturesController, action='GET_INDEX', conditions=dict(method=['GET']))

    dispatcher.connect('type_get', '/types/:type_ID', controller=typeController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('type_get_index', '/type/', controller=typeController, action='GET_INDEX', conditions=dict(method=['GET']))

    dispatcher.connect('reset_put_index', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))

    # CORS
    dispatcher.connect('party_options', '/party/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('party_id_options', '/party/:slot_ID', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('pokemon_options', '/pokemon/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('pokemon_natdexID_options', '/pokemon/:dex_ID', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('abilties_options', '/abilities/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('abilities_name_options', '/abilities/:ability_ID', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('item_options', '/items/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('item_name_options', '/items/:item_ID', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('moves_options', '/moves/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('moves_name_options', '/moves/:move_ID', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('nature_options', '/natures/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('nature_name_options', '/natures/:nature_ID', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('type_options', '/types/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('type_id_options', '/types/:type_ID', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('reset_options', '/reset/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    
    conf = {
        'global' : {
            'server.thread_pool': 5,
            'server.socket_host' : 'localhost',
            'server.socket_port': 9090
        },
        '/':{
            'request.dispatch' : dispatcher,
            'tools.CORS.on' : True,
        }
    }

    cherrypy.config.update(conf)
    app = Flask(__name__)
    cherrypy.quickstart(app)

class optionsController:
    def OPTIONS(self, *args, **kwards):
        return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()