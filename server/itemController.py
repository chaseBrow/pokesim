import re, json
from pokemon_library import _pokemon_database

class ItemController(object):

    def __init__(self, pdb=None):
        if pdb is None:
            self.pdb = _pokemon_database()
        else:
            self.pdb = pdb
        
        self.pdb.load_items('items.csv')
    
    def GET_KEY(self, item_ID):
        output = {'result':'success'}
        
        item_ID = int(item_ID)
        try:
            item = self.pdb.get_item(item_ID)
            if item is not None:
                output['item_object'] = item
            else:
                output['result'] = 'error'
                output['message'] = 'item not found'
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
    
    def GET_INDEX(self):
        output = {'result':'success'}
        output['items_list'] = []

        try:
            for item_ID in self.pdb.get_items():
                item = self.pdb.get_item(item_ID)
                output['items_list'].append(item)
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        
        return json.dumps(output)   