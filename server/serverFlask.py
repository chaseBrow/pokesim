from flask import Flask, request
from partyController import PartyController
from pokemonController import PokemonController
from abilitiesController import AbilitiesController
from itemController import ItemController
from movesController import MovesController
from naturesController import NaturesController
from typeController import TypeController
from resetController import ResetController
from pokemon_library import _pokemon_database
from flask_cors import CORS

pdb = _pokemon_database()

partyController = PartyController(pdb=pdb)
pokemonController = PokemonController(pdb=pdb)
abilitiesController = AbilitiesController(pdb=pdb)
itemController = ItemController(pdb=pdb)
movesController = MovesController(pdb=pdb)
naturesController = NaturesController(pdb=pdb)
typeController = TypeController(pdb=pdb)
resetController = ResetController(pdb=pdb)

app = Flask(__name__)
CORS(app)
# app.debug = True

@app.route('/party/', methods=['GET','DELETE'])
def party_index():
    if request.method == 'GET':
        return partyController.GET_INDEX()
    else:
        return partyController.DELETE_INDEX()

@app.route('/party/<slot_ID>', methods=['GET','POST','PUT','DELETE'])
def party_key(slot_ID):
    flask_request = request.get_json()

    if request.method == 'GET':
        return partyController.GET_KEY(slot_ID)
    elif request.method == 'POST':
        return partyController.POST_KEY(slot_ID, flask_request)
    elif request.method == 'PUT':
        return partyController.PUT_KEY(slot_ID, flask_request)
    else:
        return partyController.DELETE_KEY(slot_ID)


@app.route('/pokemon/', methods=['GET'])
def pokemon_index():
    return pokemonController.GET_INDEX()

@app.route('/pokemon/<dex_ID>', methods=['GET'])
def pokemon_key(dex_ID):
    return pokemonController.GET_KEY(dex_ID)


@app.route('/abilities/', methods=['GET'])
def abilities_index():
    return abilitiesController.GET_INDEX()

@app.route('/abilities/<ability_ID>', methods=['GET'])
def abilities_key(ability_ID):
    return abilitiesController.GET_KEY(ability_ID)


@app.route('/items/', methods=['GET'])
def items_index():
    return itemController.GET_INDEX()

@app.route('/items/<item_ID>', methods=['GET'])
def items_key(item_ID):
    return itemController.GET_KEY(item_ID)


@app.route('/moves/', methods=['GET'])
def moves_index():
    return movesController.GET_INDEX()

@app.route('/moves/<move_ID>', methods=['GET'])
def moves_key(move_ID):
    return movesController.GET_KEY(move_ID)


@app.route('/natures/', methods=['GET'])
def natures_index():
    return naturesController.GET_INDEX()

@app.route('/natures/<nature_ID>', methods=['GET'])
def natures_key(nature_ID):
    return naturesController.GET_KEY(nature_ID)


@app.route('/types/', methods=['GET'])
def types_index():
    return typeController.GET_INDEX()

@app.route('/types/<type_ID>', methods=['GET'])
def types_key(type_ID):
    return typeController.GET_KEY(type_ID)


@app.route('/reset/', methods=['PUT'])
def reset():
    return resetController.PUT_INDEX()

if __name__ == '__main__':
    app.run(host='localhost', port=3080)
