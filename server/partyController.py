import re, json
from pokemon_library import _pokemon_database

class PartyController(object):

    def __init__(self, pdb=None):
        if pdb is None:
            self.pdb = _pokemon_database()
        else:
            self.pdb = pdb
        
        self.pdb.load_abilities('abilities.csv')
        self.pdb.load_items('items.csv')
        self.pdb.load_moves('moves.csv')
        self.pdb.load_natures('natures.csv')
        self.pdb.load_pokemon('pokemonandmoves.csv')

    def GET_KEY(self, slot_ID):
        output = {'result':'success'}
        slot_ID = str(slot_ID)

        try:
            party_member = self.pdb.get_party_member(slot_ID)
            if party_member is not None:
                output['party placement'] = slot_ID
                output['member info'] = party_member
            else:
                output['result'] = 'error'
                output['message'] = 'party member not found'
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        
        return json.dumps(output)

    def PUT_KEY(self, slot_ID,flask_request):
        output = {'result':'success'}
        slot_ID = str(slot_ID)

        data = flask_request

        if 'gender' in data:
            if not self.pdb.set_party_member_gender(slot_ID,str(data['gender'])):
                output['result'] = 'error'
                output['message'] = 'invalid gender'

        if 'nature_ID' in data:
            if not self.pdb.set_party_member_nature(slot_ID,int(data['nature_ID'])):
                output['result'] = 'error'
                output['message'] = 'invalid nature'

        if 'ability_ID' in data:
            if not self.pdb.set_party_member_ability(slot_ID,int(data['ability_ID'])):
                output['result'] = 'error'
                output['message'] = 'invalid ability'
        
        if 'item_ID' in data:
            if not self.pdb.set_party_member_item(slot_ID,int(data['item_ID'])):
                output['result'] = 'error'
                output['message'] = 'invalid item'

        if 'move1_ID' in data:
            if not self.pdb.set_party_member_move(slot_ID,1,int(data['move1_ID'])):
                output['result'] = 'error'
                output['message'] = 'invalid move 1'

        if 'move2_ID' in data:
            if not self.pdb.set_party_member_move(slot_ID,2,int(data['move2_ID'])):
                output['result'] = 'error'
                output['message'] = 'invalid move 2'

        if 'move3_ID' in data:
            if not self.pdb.set_party_member_move(slot_ID,3,int(data['move3_ID'])):
                output['result'] = 'error'
                output['message'] = 'invalid move 3'

        if 'move4_ID' in data:
            if not self.pdb.set_party_member_move(slot_ID,4,int(data['move4_ID'])):
                output['result'] = 'error'
                output['message'] = 'invalid move 4'

        return json.dumps(output)

    def POST_KEY(self,slot_ID,flask_request):
        output = {'result':'success'}
        data = flask_request
        try:
            if self.pdb.get_party_member(slot_ID) == None:
                if not self.pdb.set_party_member(str(slot_ID),int(data['dex_ID']),str(data['form_name']).title(),str(data['gender']).title(),int(data['nature_ID']),int(data['ability_ID']),int(data['item_ID']),int(data['move1_ID']),int(data['move2_ID']),int(data['move3_ID']),int(data['move4_ID'])):
                    output['message'] = 'Invalid Member Specifications'
            else:
                output['message'] = 'Party Slot is Full'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        
        return json.dumps(output)

    def DELETE_KEY(self, slot_ID):
        output = {'result':'success'}
        slot_ID = int(slot_ID)

        try:
            self.pdb.delete_party_member(slot_ID)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        
        return json.dumps(output)

    def GET_INDEX(self):
        output = {'result':'success'}

        try:
            output['party members'] = self.pdb.get_party_members()
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        
        return json.dumps(output)
    
    def DELETE_INDEX(self):
        output = {'result':'success'}

        try:
            self.pdb.delete_party()
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        
        return json.dumps(output)
        
    