import unittest
from pokemon_library import _pokemon_database

class TestLibrary(unittest.TestCase):

    print("Testing for library pokemon_library")
    def create_database(self, pdb=None):
        if pdb is None:
            self.pdb = _pokemon_database()
        else:
            self.pdb = pdb
    
    def load_csv(self):
        self.pdb.load_abilities('abilities.csv')
        self.pdb.load_items('items.csv')
        self.pdb.load_moves('moves.csv')
        self.pdb.load_natures('natures.csv')
        self.pdb.load_pokemon('pokemonandmoves.csv')
        self.pdb.load_typechart('types.csv')

    def test_get_abilties(self):
        #Check for length of keys to see if all keys exist
        key_list = self.pdb.get_abilities()
        self.assertEqual(len(key_list),167)

    def test_get_ability(self):
        #Test specific ability details for specified ID
        testability = self.pdb.get_ability('58')
        
        self.assertEqual(testability['ID'],'58')
        self.assertEqual(testability['Name'],'Levitate')
        self.assertEqual(testability['Description'],'This Pokemon is immune to Ground; Gravity/Ingrain/Smack Down/Iron Ball nullify it.')

    def test_get_items(self):
        #Check for length of keys to see if all keys exist
        key_list = self.pdb.get_items()
        self.assertEqual(len(key_list),250)

    def test_get_item(self):
        #Test specific item details for specified ID
        testitem = self.pdb.get_item('13')
        
        self.assertEqual(testitem['ID'],'13')
        self.assertEqual(testitem['Name'],'Black Belt')
        self.assertEqual(testitem['Description'],'Holder\'s Fighting-type attacks have 1.2x power.')

    def test_get_moves(self):
        #Check for length of keys to see if all keys exist
        key_list = self.pdb.get_moves()
        self.assertEqual(len(key_list),561)

    def test_get_move(self):
        #Test specific move details for specified ID
        testmove = self.pdb.get_move('120')
        
        self.assertEqual(testmove['ID'],'120')
        self.assertEqual(testmove['Name'],'Spore')
        self.assertEqual(testmove['Type'],'Grass')
        self.assertEqual(testmove['Category'],'Non-Damaging')
        self.assertEqual(testmove['Power'],'0')
        self.assertEqual(testmove['Accuracy'],'100')
        self.assertEqual(testmove['Priority'],'0')
        self.assertEqual(testmove['PP'],'15')
        self.assertEqual(testmove['Description'],'Causes the target to fall asleep.')
    
    def test_get_natures(self):
        #Check for length of keys to see if all keys exist
        key_list = self.pdb.get_natures()
        self.assertEqual(len(key_list),25)

    def test_get_nature(self):
        #Test specific nature details for specified ID
        testnature = self.pdb.get_nature('16')
        
        self.assertEqual(testnature['ID'],'16')
        self.assertEqual(testnature['Name'],'Modest')
        self.assertEqual(testnature['Summary'],'"+SpA, -Atk"')
        self.assertEqual(testnature['HP'],'1')
        self.assertEqual(testnature['ATK'],'0.9')
        self.assertEqual(testnature['DEF'],'1')
        self.assertEqual(testnature['SPATK'],'1.1')
        self.assertEqual(testnature['SPDEF'],'1')
        self.assertEqual(testnature['SPE'],'1')

    def test_get_pokemon_all(self):
        #Check for length of keys to see if all keys exist
        key_list = self.pdb.get_pokemon_all()
        self.assertEqual(len(key_list),649)

    def test_get_pokemon_one(self):
        #Test specific pokemon details for specified ID
        testpokemon = self.pdb.get_pokemon_one('88')
        
        self.assertEqual(testpokemon['Form Name'],'Grimer')
        self.assertEqual(testpokemon['Info']['ID'],'88')
        self.assertEqual(testpokemon['Info']['Name'],'Grimer')
        self.assertEqual(testpokemon['Info']['Alt_Form'],'')
        self.assertEqual(testpokemon['Info']['Types'],'Poison')
        self.assertEqual(testpokemon['Info']['Abilities'],'"[\'Poison Touch\', \'Stench\', \'Sticky Hold\']"')
        self.assertEqual(testpokemon['Info']['HP'],'80')
        self.assertEqual(testpokemon['Info']['ATK'],'80')
        self.assertEqual(testpokemon['Info']['DEF'],'50')
        self.assertEqual(testpokemon['Info']['SPATK'],'40')
        self.assertEqual(testpokemon['Info']['SPDEF'],'50')
        self.assertEqual(testpokemon['Info']['SPE'],'25')
        self.assertEqual(testpokemon['Info']['Evolves_Into'],'Muk')
        self.assertEqual(testpokemon['Info']['Learnable_Moves'],'"[\'Acid Armor\', \'Acid Spray\', \'Attract\', \'Body Slam\', \'Captivate\', \'Curse\', \'Dig\', \'Disable\', \'Double Team\', \'Dynamic Punch\', \'Endure\', \'Explosion\', \'Facade\', \'Fire Blast\', \'Fire Punch\', \'Flamethrower\', \'Fling\', \'Frustration\', \'Giga Drain\', \'Gunk Shot\', \'Harden\', \'Haze\', \'Helping Hand\', \'Hidden Power\', \'Ice Punch\', \'Imprison\', \'Incinerate\', \'Lick\', \'Mean Look\', \'Memento\', \'Mimic\', \'Minimize\', \'Mud Bomb\', \'Mud-Slap\', \'Natural Gift\', \'Pain Split\', \'Payback\', \'Poison Gas\', \'Poison Jab\', \'Pound\', \'Protect\', \'Rain Dance\', \'Rest\', \'Return\', \'Rock Slide\', \'Rock Tomb\', \'Round\, \'Scary Face\', \'Screech\', \'Secret Power\', \'Self-Destruct\', \'Shadow Ball\', \'Shadow Punch\', \'Shadow Sneak\', \'Shock Wave\', \'Sleep Talk\', \'Sludge\', \'Sludge Bomb\', \'Sludge Wave\', \'Snore\', \'Spit Up\', \'Stockpile\', \'Strength\', \'Substitute\', \'Sunny Day\', \'Swagger\', \'Swallow\', \'Taunt\', \'Thief\', \'Thunder\', \'Thunderbolt\', \'Thunder Punch\', \'Torment\', \'Toxic\', \'Venoshock\']"')

    def test_get_pokemon_form(self):
        #Test specific pokemon details for specified ID and form
        testpokemon = self.pdb.get_pokemon_form('492','Shaymin-Sky')
        
        self.assertEqual(testpokemon['Form Name'],'Shaymin-Sky')
        self.assertEqual(testpokemon['Info']['ID'],'492')
        self.assertEqual(testpokemon['Info']['Name'],'Shaymin-Sky')
        self.assertEqual(testpokemon['Info']['Alt_Form'],'')
        self.assertEqual(testpokemon['Info']['Types'],'"[\'Grass\', \'Flying\']"')
        self.assertEqual(testpokemon['Info']['Abilities'],'\'Serene Grace\'')
        self.assertEqual(testpokemon['Info']['HP'],'100')
        self.assertEqual(testpokemon['Info']['ATK'],'103')
        self.assertEqual(testpokemon['Info']['DEF'],'75')
        self.assertEqual(testpokemon['Info']['SPATK'],'120')
        self.assertEqual(testpokemon['Info']['SPDEF'],'75')
        self.assertEqual(testpokemon['Info']['SPE'],'127')
        self.assertEqual(testpokemon['Info']['Evolves_Into'],'')
        self.assertEqual(testpokemon['Info']['Learnable_Moves'],'"[\'Air Cutter\', \'Air Slash\', \'Aromatherapy\', \'Bullet Seed\', \'Covet\', \'Defense Curl\', \'Double Team\', \'Earth Power\', \'Endeavor\', \'Endure\', \'Energy Ball\', \'Facade\', \'Flash\', \'Frustration\', \'Giga Drain\', \'Giga Impact\', \'Grass Knot\', \'Grass Whistle\', \'Growth\', \'Headbutt\', \'Healing Wish\', \'Hidden Power\', \'Hyper Beam\', \'Last Resort\', \'Leaf Storm\', \'Leech Seed\', \'Lucky Chant\', \'Magical Leaf\', \'Mud-Slap\', \'Natural Gift\', \'Ominous Wind\', \'Protect\', \'Psychic\', \'Psych Up\', \'Quick Attack\', \'Rest\', \'Return\', \'Round\', \Safeguard\', \'Secret Power\', \'Seed Bomb\', \'Seed Flare\', \'Sleep Talk\', \'Snore\', \'Solar Beam\', \'Substitute\', \'Sunny Day\', \'Swagger\', \'Sweet Kiss\', \'Sweet Scent\', \'Swift\', \'Swords Dance\', \'Synthesis\', \'Tailwind\', \'Toxic\', \'Worry Seed\', \'Zen Headbutt\']"')


    def test_get_typecharts(self):
        #Check for length of keys to see if all keys exist
        key_list = self.pdb.get_typecharts()
        self.assertEqual(len(key_list),17)

    def test_get_typechart(self):
        #Test specific typing details for specified ID
        testtypechart = self.pdb.get_typechart('11')

        self.assertEqual(testtypechart['ID'],'11')
        self.assertEqual(testtypechart['Name'],'Ice')
        self.assertEqual(testtypechart['Atk_Effectiveness'],'"[[\'Bug\', 1], [\'Dark\', 1], [\'Dragon\', 2], [\'Electric\', 1], [\'Fighting\', 1], [\'Fire\', 0.5], [\'Flying\', 2], [\'Ghost\', 1], [\'Grass\', 2], [\'Ground\', 2], [\'Ice\', 0.5], [\'Normal\', 1], [\'Poison\', 1], [\'Psychic\', 1], [\'Rock\', 1], [\'Steel\', 0.5], [\'Water\', 0.5]]"')
        self.assertEqual(testtypechart['Description'],'Pokemon of this type cannot become frozen and are immune to damage from Hail.')



if __name__ == "__main__":
    unittest.main()