import unittest
import requests
import json

class TestIndex(unittest.TestCase):

    SITE_URL = 'http://localhost:51026'
    print("Testing for server: " + SITE_URL)
    ABILITY_URL = SITE_URL + '/abilities/'
    ITEM_URL = SITE_URL + '/items/'
    MOVE_URL = SITE_URL + '/moves/'
    MOVESET_URL = SITE_URL + '/moveset/'
    NATURE_URL = SITE_URL + '/natures/'
    POKEMON_URL = SITE_URL + '/pokemon/'
    TYPE_URL = SITE_URL + '/type/'
    PARTY_URL = SITE_URL + '/party/'
    SAVESTATE_URL = SITE_URL + '/save/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_party_save_data(self):
        m = {}
        r = requests.put(self.RESET_URL, json.dumps(m))
    
    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False
    
    def test_abilities_get_index(self):
        r = requests.get(self.ABILITY_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testability = {}
        abilities = resp['abilities']
        for ability in abilities:
            if ability['ability name'] == 'Damp':
                testability = ability

        self.assertEqual(testability['ability name'],'Damp')
        self.assertEqual(testability['ability description'],'Prevents the use of explosive moves such as Self-Destruct by dampening its surroundings.')

    def test_items_get_index(self):
        r = requests.get(self.ITEM_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testitem = {}
        items = resp['items']
        for item in items:
            if item['item name'] == 'Master Ball':
                testitem = item

        self.assertEqual(testitem['item name'],'Master Ball')
        self.assertEqual(testitem['item description'],'The best Poke Ball with the ultimate level of performance. With it, you will catch any wild Pokemon without fail.')

    def test_moves_get_index(self):
        r = requests.get(self.MOVE_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testmove = {}
        moves = resp['moves']
        for move in moves:
            if move['move name'] == 'Karate Chop':
                testmove = move

        self.assertEqual(testmove['move description'],'The target is attacked with a sharp chop. Critical hits land more easily.')
        self.assertEqual(testmove['move type'],'Fighting')
        self.assertEqual(testmove['move category'],'Physical')
        self.assertEqual(testmove['move power'],50)
        self.assertEqual(testmove['move accuracy'],'100%')
        self.assertEqual(testmove['move pp'],25)
        self.assertEqual(testmove['move zeffect'],'100 Power')
        self.assertEqual(testmove['priority'],0)
        self.assertEqual(testmove['crit chance multiplier'],1)

    def test_moveset_get_index(self):
        r = requests.get(self.MOVESET_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testmoveset = {}
        movesets = resp['movesets']
        for moveset in movesets:
            if moveset['pokemon form'] == 'Charmander':
                testmoveset = moveset

        self.assertEqual(testmoveset['moveset'],['Start - Scratch', 'Start - Growl', 'L7 - Ember', 'L10 - Smokescreen', 'L16 - Dragon Rage', 'L19 - Scary Face', 'L25 - Fire Fang', 'L28 - Flame Burst', 'L34 - Slash', 'L37 - Flamethrower', 'L43 - Fire Spin', 'L46 - Inferno', 'TM01 - Work Up', 'TM02 - Dragon Claw', 'TM06 - Toxic', 'TM10 - Hidden Power', 'TM11 - Sunny Day', 'TM17 - Protect', 'TM21 - Frustration', 'TM27 - Return', 'TM31 - Brick Break', 'TM32 - Double Team', 'TM35 - Flamethrower', 'TM38 - Fire Blast', 'TM39 - Rock Tomb', 'TM40 - Aerial Ace', 'TM42 - Facade', 'TM43 - Flame Charge', 'TM44 - Rest', 'TM45 - Attract', 'TM48 - Round', 'TM49 - Echoed Voice', 'TM50 - Overheat', 'TM56 - Fling', 'TM61 - Will-O-Wisp', 'TM65 - Shadow Claw', 'TM75 - Swords Dance', 'TM80 - Rock Slide', 'TM87 - Swagger', 'TM88 - Sleep Talk', 'TM90 - Substitute', 'TM100 - Confide', 'Tutor - Blast Burn', 'Egg - Belly Drum', 'Egg - Ancient Power', 'Egg - Bite', 'Egg - Outrage', 'Egg - Beat Up', 'Egg - Dragon Dance', 'Egg - Crunch', 'Egg - Dragon Rush', 'Egg - Metal Claw', 'Egg - Flare Blitz', 'Egg - Counter', 'Egg - Dragon Pulse', 'Egg - Focus Punch', 'Egg - Air Cutter', 'ORAS - Thunder Punch', 'ORAS - Fire Punch', 'ORAS - Dragon Pulse', 'ORAS - Iron Tail', 'ORAS - Snore', 'ORAS - Heat Wave', 'ORAS - Focus Punch'])

    def test_natures_get_index(self):
        r = requests.get(self.NATURE_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testnature = {}
        natures = resp['natures']
        for nature in natures:
            if nature['nature name'] == 'Gentle':
                testnature = nature

        self.assertEqual(testnature['attack multiplier'],1.0)
        self.assertEqual(testnature['defense multiplier'],0.9)
        self.assertEqual(testnature['special attack multiplier'],1.0)
        self.assertEqual(testnature['special defense multiplier'],1.1)
        self.assertEqual(testnature['speed multiplier'],1.0)

    def test_pokemon_get_index(self):
        r = requests.get(self.POKEMON_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testpokemon = {}
        pokemon = resp['pokemon']
        for x in pokemon:
            if x['national dex number'] == 23:
                testpokemon = x

        self.assertEqual(testpokemon['pokemon form'],'Ekans')
        self.assertEqual(testpokemon['pokemon information'],{'id': 23, 'types': ['Poison'], 'abilities': ['Intimidate', 'Shed Skin', 'Unnerve'], 'base stats': {'hp': 35, 'attack': 60, 'defense': 44, 'spattack': 40, 'spdefense': 54, 'speed': 55, 'total': 288}, 'classification': 'Snake Pokemon', 'pre-evolution': 'None'})

    def test_type_get_index(self):
        r = requests.get(self.TYPE_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testtypechart = {}
        typecharts = resp['typecharts']
        for typechart in typecharts:
            if typechart['typing id'] == 7:
                testtypechart = typechart

        self.assertEqual(testtypechart['defensive typing'],['normal', 'poison'])
        self.assertEqual(testtypechart['normal multiplier'],1.0)
        self.assertEqual(testtypechart['fire multiplier'],1.0)
        self.assertEqual(testtypechart['water multiplier'],1.0)
        self.assertEqual(testtypechart['electric multiplier'],1.0)
        self.assertEqual(testtypechart['grass multiplier'],0.5)
        self.assertEqual(testtypechart['ice multiplier'],1.0)
        self.assertEqual(testtypechart['fighting multiplier'],1.0)
        self.assertEqual(testtypechart['poison multiplier'],0.5)
        self.assertEqual(testtypechart['ground multiplier'],2.0)
        self.assertEqual(testtypechart['flying multiplier'],1.0)
        self.assertEqual(testtypechart['psychic multiplier'],2.0)
        self.assertEqual(testtypechart['bug multiplier'],0.5)
        self.assertEqual(testtypechart['rock multiplier'],1.0)
        self.assertEqual(testtypechart['ghost multiplier'],0.0)
        self.assertEqual(testtypechart['dragon multiplier'],1.0)
        self.assertEqual(testtypechart['dark multiplier'],1.0)
        self.assertEqual(testtypechart['steel multiplier'],1.0)
        self.assertEqual(testtypechart['fairy multiplier'],0.5)

    def test_party_post_index(self):
        self.reset_party_save_data()

        p = {}
        p['natdexID'] = 8
        p['form_name'] = 'Wartortle'
        p['nature_name'] = 'Calm'
        p['ability_name'] = 'Torrent'
        p['item_name'] = 'Potion'
        p['move1_name'] = 'Bubble'
        p['move2_name'] = 'Tackle'
        p['move3_name'] = 'Withdraw'
        p['move4_name'] = 'Tail Whip'
        r = requests.post(self.PARTY_URL, data=json.dumps(p))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['slot number'], 1)

        r = requests.get(self.PARTY_URL + str(resp['slot number']))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        resp = resp['member info']
        self.assertEqual(resp['natdexID'], p['natdexID'])
        self.assertEqual(resp['form_name'], p['form_name'])
        self.assertEqual(resp['nature_name'], p['nature_name'])
        self.assertEqual(resp['ability_name'], p['ability_name'])
        self.assertEqual(resp['item_name'], p['item_name'])
        self.assertEqual(resp['move1_name'], p['move1_name'])
        self.assertEqual(resp['move2_name'], p['move2_name'])
        self.assertEqual(resp['move3_name'], p['move3_name'])
        self.assertEqual(resp['move4_name'], p['move4_name'])

    def test_party_get_index(self):
        self.reset_party_save_data()

        p = {}
        p['natdexID'] = 8
        p['form_name'] = 'Wartortle'
        p['nature_name'] = 'Calm'
        p['ability_name'] = 'Torrent'
        p['item_name'] = 'Potion'
        p['move1_name'] = 'Bubble'
        p['move2_name'] = 'Tackle'
        p['move3_name'] = 'Withdraw'
        p['move4_name'] = 'Tail Whip'
        r = requests.post(self.PARTY_URL, data=json.dumps(p))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.PARTY_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testpartymem = {}
        partymembers = resp['party members']
        for partymember in partymembers:
            if partymember['slot number'] == 1:
                testpartymem = partymember

        self.assertEqual(testpartymem['party member info'],{'natdexID': 8, 'form_name': 'Wartortle', 'types': ['Water'], 'nature_name': 'Calm', 'ability_name': 'Torrent', 'item_name': 'Potion', 'move1_name': 'Bubble', 'move2_name': 'Tackle', 'move3_name': 'Withdraw', 'move4_name': 'Tail Whip'}) 
    
    def test_party_delete_index(self):
        self.reset_party_save_data()

        message = {}
        p = {}
        p['natdexID'] = 8
        p['form_name'] = 'Wartortle'
        p['nature_name'] = 'Calm'
        p['ability_name'] = 'Torrent'
        p['item_name'] = 'Potion'
        p['move1_name'] = 'Bubble'
        p['move2_name'] = 'Tackle'
        p['move3_name'] = 'Withdraw'
        p['move4_name'] = 'Tail Whip'
        r = requests.post(self.PARTY_URL, data=json.dumps(p))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.delete(self.PARTY_URL, data = json.dumps(message))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.PARTY_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['message'], 'Party is empty')

    def test_savestate_post_index(self):
        self.reset_party_save_data()

        p = {}
        p['natdexID'] = 8
        p['form_name'] = 'Wartortle'
        p['nature_name'] = 'Calm'
        p['ability_name'] = 'Torrent'
        p['item_name'] = 'Potion'
        p['move1_name'] = 'Bubble'
        p['move2_name'] = 'Tackle'
        p['move3_name'] = 'Withdraw'
        p['move4_name'] = 'Tail Whip'
        r = requests.post(self.PARTY_URL, data=json.dumps(p))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.post(self.SAVESTATE_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SAVESTATE_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        resp = resp['saved states'][0]

        self.assertEqual(resp['saved party member info']['natdexID'], p['natdexID'])
        self.assertEqual(resp['saved party member info']['form_name'], p['form_name'])
        self.assertEqual(resp['saved party member info']['nature_name'], p['nature_name'])
        self.assertEqual(resp['saved party member info']['ability_name'], p['ability_name'])
        self.assertEqual(resp['saved party member info']['item_name'], p['item_name'])
        self.assertEqual(resp['saved party member info']['move1_name'], p['move1_name'])
        self.assertEqual(resp['saved party member info']['move2_name'], p['move2_name'])
        self.assertEqual(resp['saved party member info']['move3_name'], p['move3_name'])
        self.assertEqual(resp['saved party member info']['move4_name'], p['move4_name'])


    def test_savestate_get_index(self):
        self.reset_party_save_data()

        p = {}
        p['natdexID'] = 8
        p['form_name'] = 'Wartortle'
        p['nature_name'] = 'Calm'
        p['ability_name'] = 'Torrent'
        p['item_name'] = 'Potion'
        p['move1_name'] = 'Bubble'
        p['move2_name'] = 'Tackle'
        p['move3_name'] = 'Withdraw'
        p['move4_name'] = 'Tail Whip'
        r = requests.post(self.PARTY_URL, data=json.dumps(p))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        q = {}
        q['natdexID'] = 4
        q['form_name'] = 'Charmander'
        q['nature_name'] = 'Brave'
        q['ability_name'] = 'Blaze'
        q['item_name'] = 'Potion'
        q['move1_name'] = 'Ember'
        q['move2_name'] = 'Scratch'
        q['move3_name'] = 'Growl'
        q['move4_name'] = 'Smokescreen'
        r = requests.post(self.PARTY_URL, data=json.dumps(q))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.post(self.SAVESTATE_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SAVESTATE_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        testsavemem = {}
        savemembers = resp['saved states']
        for savemember in savemembers:
            if savemember['save slot number'] == 2:
                testsavemem = savemember
        
        self.assertEqual(testsavemem['saved party member info']['natdexID'], q['natdexID'])
        self.assertEqual(testsavemem['saved party member info']['form_name'], q['form_name'])
        self.assertEqual(testsavemem['saved party member info']['nature_name'], q['nature_name'])
        self.assertEqual(testsavemem['saved party member info']['ability_name'], q['ability_name'])
        self.assertEqual(testsavemem['saved party member info']['item_name'], q['item_name'])
        self.assertEqual(testsavemem['saved party member info']['move1_name'], q['move1_name'])
        self.assertEqual(testsavemem['saved party member info']['move2_name'], q['move2_name'])
        self.assertEqual(testsavemem['saved party member info']['move3_name'], q['move3_name'])
        self.assertEqual(testsavemem['saved party member info']['move4_name'], q['move4_name'])


    def test_savestate_delete_index(self):
        self.reset_party_save_data()

        message = {}
        p = {}
        p['natdexID'] = 8
        p['form_name'] = 'Wartortle'
        p['nature_name'] = 'Calm'
        p['ability_name'] = 'Torrent'
        p['item_name'] = 'Potion'
        p['move1_name'] = 'Bubble'
        p['move2_name'] = 'Tackle'
        p['move3_name'] = 'Withdraw'
        p['move4_name'] = 'Tail Whip'
        r = requests.post(self.PARTY_URL, data=json.dumps(p))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.post(self.SAVESTATE_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.delete(self.SAVESTATE_URL, data = json.dumps(message))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SAVESTATE_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        saves = resp['saved states']
        self.assertFalse(saves)

    def test_put_reset_index(self):
        m = {}
        r = requests.put(self.RESET_URL, json.dumps(m))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        
        r = requests.get(self.SITE_URL + '/party/')
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['party members'], [])

        r = requests.get(self.SITE_URL + '/save/')
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        for slot in resp['saved states']:
            self.assertEqual(slot['saved party member info'], None)
		
if __name__ == "__main__":
    unittest.main()
    

    
