import unittest
import requests
import json

class TestKey(unittest.TestCase):

    SITE_URL = 'http://localhost:51026'
    print("Testing for server: " + SITE_URL)
    ABILITY_URL = SITE_URL + '/abilities/'
    ITEM_URL = SITE_URL + '/items/'
    MOVE_URL = SITE_URL + '/moves/'
    MOVESET_URL = SITE_URL + '/moveset/'
    NATURE_URL = SITE_URL + '/natures/'
    POKEMON_URL = SITE_URL + '/pokemon/'
    TYPE_URL = SITE_URL + '/type/'
    PARTY_URL = SITE_URL + '/party/'
    SAVESTATE_URL = SITE_URL + '/save/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_party_save_data(self):
        m = {}
        r = requests.put(self.RESET_URL, json.dumps(m))
    
    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False
    
    def test_abilities_get_key(self):
        ability_name = 'Compound'+'%20'+'Eyes'
        r = requests.get(self.ABILITY_URL + str(ability_name))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['ability name'],'Compound Eyes')
        self.assertEqual(resp['ability description'],'The Pokemon\'s compound eyes boost its accuracy.')

    def test_items_get_key(self):
        item_name = 'Life'+'%20'+'Orb'
        r = requests.get(self.ITEM_URL + str(item_name))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['item name'],'Life Orb')
        self.assertEqual(resp['item description'],'An item to be held by a Pokemon. It boosts the power of moves, but at the cost of some HP on each hit.')

    def test_moves_get_key(self):
        move_name = 'Ember'
        r = requests.get(self.MOVE_URL + str(move_name))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['move description'],'The target is attacked with small flames. This may also leave the target with a burn.')
        self.assertEqual(resp['move type'],'Fire')
        self.assertEqual(resp['move category'],'Special')
        self.assertEqual(resp['move power'],40)
        self.assertEqual(resp['move accuracy'],'100%')
        self.assertEqual(resp['move pp'],25)
        self.assertEqual(resp['move zeffect'],'100 Power')
        self.assertEqual(resp['priority'],0)
        self.assertEqual(resp['crit chance multiplier'],0)

    def test_moveset_get_key(self):
        natdexID = 4
        r = requests.get(self.MOVESET_URL + str(natdexID))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['species movesets'][0]['moveset'],['Start - Scratch', 'Start - Growl', 'L7 - Ember', 'L10 - Smokescreen', 'L16 - Dragon Rage', 'L19 - Scary Face', 'L25 - Fire Fang', 'L28 - Flame Burst', 'L34 - Slash', 'L37 - Flamethrower', 'L43 - Fire Spin', 'L46 - Inferno', 'TM01 - Work Up', 'TM02 - Dragon Claw', 'TM06 - Toxic', 'TM10 - Hidden Power', 'TM11 - Sunny Day', 'TM17 - Protect', 'TM21 - Frustration', 'TM27 - Return', 'TM31 - Brick Break', 'TM32 - Double Team', 'TM35 - Flamethrower', 'TM38 - Fire Blast', 'TM39 - Rock Tomb', 'TM40 - Aerial Ace', 'TM42 - Facade', 'TM43 - Flame Charge', 'TM44 - Rest', 'TM45 - Attract', 'TM48 - Round', 'TM49 - Echoed Voice', 'TM50 - Overheat', 'TM56 - Fling', 'TM61 - Will-O-Wisp', 'TM65 - Shadow Claw', 'TM75 - Swords Dance', 'TM80 - Rock Slide', 'TM87 - Swagger', 'TM88 - Sleep Talk', 'TM90 - Substitute', 'TM100 - Confide', 'Tutor - Blast Burn', 'Egg - Belly Drum', 'Egg - Ancient Power', 'Egg - Bite', 'Egg - Outrage', 'Egg - Beat Up', 'Egg - Dragon Dance', 'Egg - Crunch', 'Egg - Dragon Rush', 'Egg - Metal Claw', 'Egg - Flare Blitz', 'Egg - Counter', 'Egg - Dragon Pulse', 'Egg - Focus Punch', 'Egg - Air Cutter', 'ORAS - Thunder Punch', 'ORAS - Fire Punch', 'ORAS - Dragon Pulse', 'ORAS - Iron Tail', 'ORAS - Snore', 'ORAS - Heat Wave', 'ORAS - Focus Punch'])

    def test_natures_get_key(self):
        nature_name = 'Adamant'
        r = requests.get(self.NATURE_URL + str(nature_name))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['attack multiplier'],1.1)
        self.assertEqual(resp['defense multiplier'],1.0)
        self.assertEqual(resp['special attack multiplier'],0.9)
        self.assertEqual(resp['special defense multiplier'],1.0)
        self.assertEqual(resp['speed multiplier'],1.0)

    def test_pokemon_get_key(self):
        natdexID = 4
        r = requests.get(self.POKEMON_URL + str(natdexID))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['pokemon form'],'Charmander')
        self.assertEqual(resp['pokemon information'],{'id': 4, 'types': ['Fire'], 'abilities': ['Blaze', 'Solar Power'], 'base stats': {'hp': 39, 'attack': 52, 'defense': 43, 'spattack': 60, 'spdefense': 50, 'speed': 65, 'total': 309}, 'classification': 'Lizard Pokemon', 'pre-evolution': 'None'})

    def test_type_get_key(self):
        type_id = 7
        r = requests.get(self.TYPE_URL + str(type_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['defensive typing'],['normal', 'poison'])
        self.assertEqual(resp['normal multiplier'],1.0)
        self.assertEqual(resp['fire multiplier'],1.0)
        self.assertEqual(resp['water multiplier'],1.0)
        self.assertEqual(resp['electric multiplier'],1.0)
        self.assertEqual(resp['grass multiplier'],0.5)
        self.assertEqual(resp['ice multiplier'],1.0)
        self.assertEqual(resp['fighting multiplier'],1.0)
        self.assertEqual(resp['poison multiplier'],0.5)
        self.assertEqual(resp['ground multiplier'],2.0)
        self.assertEqual(resp['flying multiplier'],1.0)
        self.assertEqual(resp['psychic multiplier'],2.0)
        self.assertEqual(resp['bug multiplier'],0.5)
        self.assertEqual(resp['rock multiplier'],1.0)
        self.assertEqual(resp['ghost multiplier'],0.0)
        self.assertEqual(resp['dragon multiplier'],1.0)
        self.assertEqual(resp['dark multiplier'],1.0)
        self.assertEqual(resp['steel multiplier'],1.0)
        self.assertEqual(resp['fairy multiplier'],0.5)

    def test_party_put_key(self):
        self.reset_party_save_data()

        p = {}
        p['natdexID'] = 8
        p['form_name'] = 'Wartortle'
        p['nature_name'] = 'Calm'
        p['ability_name'] = 'Torrent'
        p['item_name'] = 'Potion'
        p['move1_name'] = 'Bubble'
        p['move2_name'] = 'Tackle'
        p['move3_name'] = 'Withdraw'
        p['move4_name'] = 'Tail Whip'
        r = requests.post(self.PARTY_URL, data=json.dumps(p))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['slot number'], 1)

        slot_num = resp['slot number']
        r = requests.get(self.PARTY_URL + str(slot_num))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        resp = resp['member info']
        self.assertEqual(resp['natdexID'], p['natdexID'])
        self.assertEqual(resp['form_name'], p['form_name'])
        self.assertEqual(resp['nature_name'], p['nature_name'])
        self.assertEqual(resp['ability_name'], p['ability_name'])
        self.assertEqual(resp['item_name'], p['item_name'])
        self.assertEqual(resp['move1_name'], p['move1_name'])
        self.assertEqual(resp['move2_name'], p['move2_name'])
        self.assertEqual(resp['move3_name'], p['move3_name'])
        self.assertEqual(resp['move4_name'], p['move4_name'])

        q = {}
        q['item_name'] = 'Hyper Potion'
        q['move1_name'] = 'Bite'
        r = requests.put(self.PARTY_URL + str(slot_num), data = json.dumps(q))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.PARTY_URL + str(slot_num))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        resp = resp['member info']
        self.assertEqual(resp['natdexID'], p['natdexID'])
        self.assertEqual(resp['form_name'], p['form_name'])
        self.assertEqual(resp['nature_name'], p['nature_name'])
        self.assertEqual(resp['ability_name'], p['ability_name'])
        self.assertEqual(resp['item_name'], q['item_name'])
        self.assertEqual(resp['move1_name'], q['move1_name'])
        self.assertEqual(resp['move2_name'], p['move2_name'])
        self.assertEqual(resp['move3_name'], p['move3_name'])
        self.assertEqual(resp['move4_name'], p['move4_name'])


    def test_party_get_key(self):
        self.reset_party_save_data()

        p = {}
        p['natdexID'] = 8
        p['form_name'] = 'Wartortle'
        p['nature_name'] = 'Calm'
        p['ability_name'] = 'Torrent'
        p['item_name'] = 'Potion'
        p['move1_name'] = 'Bubble'
        p['move2_name'] = 'Tackle'
        p['move3_name'] = 'Withdraw'
        p['move4_name'] = 'Tail Whip'
        r = requests.post(self.PARTY_URL, data=json.dumps(p))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['slot number'], 1)

        slot_num = resp['slot number']
        r = requests.get(self.PARTY_URL + str(slot_num))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        resp = resp['member info']
        self.assertEqual(resp['natdexID'], p['natdexID'])
        self.assertEqual(resp['form_name'], p['form_name'])
        self.assertEqual(resp['nature_name'], p['nature_name'])
        self.assertEqual(resp['ability_name'], p['ability_name'])
        self.assertEqual(resp['item_name'], p['item_name'])
        self.assertEqual(resp['move1_name'], p['move1_name'])
        self.assertEqual(resp['move2_name'], p['move2_name'])
        self.assertEqual(resp['move3_name'], p['move3_name'])
        self.assertEqual(resp['move4_name'], p['move4_name']) 
    
    def test_party_delete_key(self):
        self.reset_party_save_data()

        message = {}
        p = {}
        p['natdexID'] = 8
        p['form_name'] = 'Wartortle'
        p['nature_name'] = 'Calm'
        p['ability_name'] = 'Torrent'
        p['item_name'] = 'Potion'
        p['move1_name'] = 'Bubble'
        p['move2_name'] = 'Tackle'
        p['move3_name'] = 'Withdraw'
        p['move4_name'] = 'Tail Whip'
        r = requests.post(self.PARTY_URL, data=json.dumps(p))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')
        slot_num = resp['slot number']

        r = requests.delete(self.PARTY_URL + str(slot_num), data = json.dumps(message))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.PARTY_URL + str(slot_num))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')

    def test_savestate_put_key(self):
        self.reset_party_save_data()

        p = {}
        p['natdexID'] = 8
        p['form_name'] = 'Wartortle'
        p['nature_name'] = 'Calm'
        p['ability_name'] = 'Torrent'
        p['item_name'] = 'Potion'
        p['move1_name'] = 'Bubble'
        p['move2_name'] = 'Tackle'
        p['move3_name'] = 'Withdraw'
        p['move4_name'] = 'Tail Whip'
        r = requests.post(self.PARTY_URL, data=json.dumps(p))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')
        slot_num = resp['slot number']

        r = requests.put(self.SAVESTATE_URL + str(slot_num))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SAVESTATE_URL + str(slot_num))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        slot_num = int(slot_num)
        slot_num -= 1

        self.assertEqual(resp['saved member info']['natdexID'], p['natdexID'])
        self.assertEqual(resp['saved member info']['form_name'], p['form_name'])
        self.assertEqual(resp['saved member info']['nature_name'], p['nature_name'])
        self.assertEqual(resp['saved member info']['ability_name'], p['ability_name'])
        self.assertEqual(resp['saved member info']['item_name'], p['item_name'])
        self.assertEqual(resp['saved member info']['move1_name'], p['move1_name'])
        self.assertEqual(resp['saved member info']['move2_name'], p['move2_name'])
        self.assertEqual(resp['saved member info']['move3_name'], p['move3_name'])
        self.assertEqual(resp['saved member info']['move4_name'], p['move4_name'])


    def test_savestate_get_key(self):
        self.reset_party_save_data()

        p = {}
        p['natdexID'] = 8
        p['form_name'] = 'Wartortle'
        p['nature_name'] = 'Calm'
        p['ability_name'] = 'Torrent'
        p['item_name'] = 'Potion'
        p['move1_name'] = 'Bubble'
        p['move2_name'] = 'Tackle'
        p['move3_name'] = 'Withdraw'
        p['move4_name'] = 'Tail Whip'
        r = requests.post(self.PARTY_URL, data=json.dumps(p))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        q = {}
        q['natdexID'] = 4
        q['form_name'] = 'Charmander'
        q['nature_name'] = 'Brave'
        q['ability_name'] = 'Blaze'
        q['item_name'] = 'Potion'
        q['move1_name'] = 'Ember'
        q['move2_name'] = 'Scratch'
        q['move3_name'] = 'Growl'
        q['move4_name'] = 'Smokescreen'
        r = requests.post(self.PARTY_URL, data=json.dumps(q))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.post(self.SAVESTATE_URL)
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        slot_num = 2
        r = requests.get(self.SAVESTATE_URL + str(slot_num))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['saved member info']['natdexID'], q['natdexID'])
        self.assertEqual(resp['saved member info']['form_name'], q['form_name'])
        self.assertEqual(resp['saved member info']['nature_name'], q['nature_name'])
        self.assertEqual(resp['saved member info']['ability_name'], q['ability_name'])
        self.assertEqual(resp['saved member info']['item_name'], q['item_name'])
        self.assertEqual(resp['saved member info']['move1_name'], q['move1_name'])
        self.assertEqual(resp['saved member info']['move2_name'], q['move2_name'])
        self.assertEqual(resp['saved member info']['move3_name'], q['move3_name'])
        self.assertEqual(resp['saved member info']['move4_name'], q['move4_name'])


    def test_savestate_delete_key(self):
        self.reset_party_save_data()

        message = {}
        p = {}
        p['natdexID'] = 8
        p['form_name'] = 'Wartortle'
        p['nature_name'] = 'Calm'
        p['ability_name'] = 'Torrent'
        p['item_name'] = 'Potion'
        p['move1_name'] = 'Bubble'
        p['move2_name'] = 'Tackle'
        p['move3_name'] = 'Withdraw'
        p['move4_name'] = 'Tail Whip'
        r = requests.post(self.PARTY_URL, data=json.dumps(p))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.post(self.SAVESTATE_URL)
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')
        
        slot_num = 1
        r = requests.delete(self.SAVESTATE_URL + str(slot_num), data = json.dumps(message))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.SAVESTATE_URL)
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'error')

if __name__ == "__main__":
    unittest.main()