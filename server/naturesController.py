import re, json
from pokemon_library import _pokemon_database

class NaturesController(object):

    def __init__(self, pdb=None):
        if pdb is None:
            self.pdb = _pokemon_database()
        else:
            self.pdb = pdb
        
        self.pdb.load_natures('natures.csv')

    def GET_KEY(self, nature_ID):
        output = {'result':'success'}
        nature_ID = int(nature_ID)
        try:
            nature = self.pdb.get_nature(nature_ID)
            if nature is not None:
                output['nature object'] = nature
            else:
                output['result'] = 'error'
                output['message'] = 'nature not found'
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
    
    def GET_INDEX(self):
        output = {'result':'success'}
        output['natures'] = []

        try:
            for nature_ID in self.pdb.get_natures():
                nature = self.pdb.get_nature(nature_ID)
                output['natures'].append(nature)
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        
        return json.dumps(output)    