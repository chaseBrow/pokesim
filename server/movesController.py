import re, json
from pokemon_library import _pokemon_database

class MovesController(object):

    def __init__(self, pdb=None):
        if pdb is None:
            self.pdb = _pokemon_database()
        else:
            self.pdb = pdb
        
        self.pdb.load_moves('moves.csv')

    def GET_KEY(self, move_ID):
        output = {'result':'success'}
        move_ID = int(move_ID)

        try:
            move = self.pdb.get_move(move_ID)
            if move is not None:
                output['move object'] = move
            else:
                output['result'] = 'error'
                output['message'] = 'move not found'
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
    
    def GET_INDEX(self):
        output = {'result':'success'}
        output['moves_list'] = []

        try:
            for move_ID in self.pdb.get_moves():
                move = self.pdb.get_move(move_ID)
                output['moves'].append(move)
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        
        return json.dumps(output)    