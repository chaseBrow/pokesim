import re, json
from pokemon_library import _pokemon_database

class AbilitiesController(object):

    def __init__(self, pdb=None):
        if pdb is None:
            self.pdb = _pokemon_database()
        else:
            self.pdb = pdb
        
        self.pdb.load_abilities('abilities.csv')

    def GET_KEY(self, ability_ID):
        output = {'result':'success'}
        ability_ID = int(ability_ID)

        try:
            ability = self.pdb.get_ability(ability_ID)
            if ability is not None:
                output['ability object'] = ability
            else:
                output['result'] = 'error'
                output['message'] = 'ability not found'
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
    
    def GET_INDEX(self):
        output = {'result':'success'}
        output['abilities list'] = []

        try:
            for ability_ID in self.pdb.get_abilities():
                ability = self.pdb.get_ability(ability_ID)
                output['abilities list'].append(ability)
        
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        
        return json.dumps(output)
        
