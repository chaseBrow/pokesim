import re, json
from pokemon_library import _pokemon_database

class ResetController(object):

    def __init__(self, pdb=None):
        if pdb is None:
            self.pdb = _pokemon_database()
        else:
            self.pdb = pdb
    
    def PUT_INDEX(self):
        output = {'result':'success'}
        try:
            self.pdb.delete_party()
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
        return json.dumps(output)
    
    