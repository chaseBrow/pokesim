import csv
import ast

class _pokemon_database:

    def __init__(self):
        # Read only data here
        self.abilities = dict()
        self.items = dict()
        self.moves = dict()
        self.natures = dict()
        self.pokemon = dict()
        self.typechart = dict()

        # Editable data here
        self.party_members = dict()
        self.party_members['1'] = None
        self.party_members['2'] = None
        self.party_members['3'] = None
        self.party_members['4'] = None
        self.party_members['5'] = None
        self.party_members['6'] = None
    
    # Loading in data from csv files

    def load_abilities(self, ability_file):
        with open(ability_file, encoding="utf8") as ability_file:
            csv_reader = csv.reader(ability_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    ability_ID = int(row[0])
                    ability_name = str(row[1])
                    ability_description = str(row[2])
                    
                    add_ability_dict = dict()
                    add_ability_dict['ID'] = ability_ID
                    add_ability_dict['Name'] = ability_name
                    add_ability_dict['Description'] = ability_description
                    self.abilities[ability_ID] = add_ability_dict

                    line_count += 1

    def load_items(self, item_file):
        with open(item_file, encoding="utf8") as item_file:
            csv_reader = csv.reader(item_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    item_ID = int(row[0])
                    item_name = str(row[1])
                    item_description = str(row[2])
                    
                    add_item_dict = dict()
                    add_item_dict['ID'] = item_ID
                    add_item_dict['Name'] = item_name
                    add_item_dict['Description'] = item_description
                    self.items[item_ID] = add_item_dict
                    
                    line_count += 1

    def load_moves(self, move_file):
        with open(move_file, encoding="utf8") as move_file:
            csv_reader = csv.reader(move_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    move_ID = int(row[0])
                    move_name = str(row[1])
                    move_type = str(row[2])
                    move_category = str(row[3])
                    move_power = int(row[4])
                    move_accuracy = int(row[5])
                    move_priority = int(row[6])
                    move_pp = int(row[7])
                    move_description = str(row[8])
                    
                    add_move_dict = dict()
                    add_move_dict['ID'] = move_ID
                    add_move_dict['Name'] = move_name
                    add_move_dict['Type'] = move_type
                    add_move_dict['Category'] = move_category
                    add_move_dict['Power'] = move_power
                    add_move_dict['Accuracy'] = move_accuracy
                    add_move_dict['Priority'] = move_priority
                    add_move_dict['PP'] = move_pp
                    add_move_dict['Description'] = move_description

                    self.moves[move_ID] = add_move_dict
                    line_count += 1

    def load_natures(self, nature_file):
        with open(nature_file, encoding="utf8") as nature_file:
            csv_reader = csv.reader(nature_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    nature_ID = int(row[0])
                    nature_name = str(row[1])
                    nature_hp = float(row[2])
                    nature_atk = float(row[3])
                    nature_def = float(row[4])
                    nature_spatk = float(row[5])
                    nature_spdef = float(row[6])
                    nature_spe = float(row[7])
                    nature_sum = str(row[8])
                
                    add_nature_dict = dict()
                    add_nature_dict['ID'] = nature_ID
                    add_nature_dict['Name'] = nature_name
                    add_nature_dict['Summary'] = nature_sum
                    add_nature_dict['HP'] = nature_hp
                    add_nature_dict['ATK'] = nature_atk
                    add_nature_dict['DEF'] = nature_def
                    add_nature_dict['SPATK'] = nature_spatk
                    add_nature_dict['SPDEF'] = nature_spdef
                    add_nature_dict['SPE'] = nature_spe
                    self.natures[nature_ID] = add_nature_dict

                    line_count += 1

    def load_pokemon(self, pokemon_file):
        with open(pokemon_file, encoding="utf8") as pokemon_file:
            csv_reader = csv.reader(pokemon_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    pokemon_dexID = int(row[0])
                    pokemon_name = str(row[1])
                    pokemon_hp = int(row[2])
                    pokemon_atk = int(row[3])
                    pokemon_def = int(row[4])
                    pokemon_spa = int(row[5])
                    pokemon_spd = int(row[6])
                    pokemon_spe = int(row[7])
                    pokemon_types = str(row[8])
                    pokemon_abilities = str(row[9])
                    pokemon_evos = str(row[10])
                    pokemon_alts = str(row[11])

                    pokemon_types = ast.literal_eval(pokemon_types)
                    pokemon_abilities = ast.literal_eval(pokemon_abilities)
                    
                    if pokemon_evos == '[]' or pokemon_evos == '':
                        pokemon_evos = list()
                    else:
                        pokemon_evos = ast.literal_eval(pokemon_evos)
                    
                    if pokemon_alts == '[]' or pokemon_alts == '':
                        pokemon_alts = list()
                    else:
                        pokemon_alts = ast.literal_eval(pokemon_alts)

                    pokemon_moveset = str(row[12])
                    pokemon_moveset = ast.literal_eval(pokemon_moveset)

                    add_poke_dict_info = dict()
                    add_poke_dict_info['ID'] = pokemon_dexID
                    add_poke_dict_info['Name'] = pokemon_name
                    add_poke_dict_info['Alt_Form'] = pokemon_alts
                    add_poke_dict_info['Types'] = pokemon_types
                    add_poke_dict_info['Abilities'] = pokemon_abilities
                    add_poke_dict_info['HP'] = pokemon_hp
                    add_poke_dict_info['ATK'] = pokemon_atk
                    add_poke_dict_info['DEF'] = pokemon_def
                    add_poke_dict_info['SPATK'] = pokemon_spa
                    add_poke_dict_info['SPDEF'] = pokemon_spd
                    add_poke_dict_info['SPE'] = pokemon_spe
                    add_poke_dict_info['Evolves_Into'] = pokemon_evos
                    add_poke_dict_info['Learnable_Moves'] = pokemon_moveset

                    add_poke_dict = dict()
                    add_poke_dict['Form_Name'] = pokemon_name
                    add_poke_dict['Info'] = add_poke_dict_info

                    if pokemon_dexID not in self.pokemon:
                        self.pokemon[pokemon_dexID] = dict()

                    self.pokemon[pokemon_dexID][pokemon_name] = add_poke_dict
                    
                    #self.pokemon[pokemon_dexID] = add_poke_dict

                    line_count += 1

    def load_typechart(self, typechart_file):
        with open(typechart_file, encoding="utf8") as typechart_file:
            csv_reader = csv.reader(typechart_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    type_ID = int(row[0])
                    type_name = str(row[1])
                    type_atkeffec = str(row[2])
                    type_desc = str(row[3])
                    
                    type_atkeffec_list = ast.literal_eval(type_atkeffec)

                    add_type_dict = dict()
                    add_type_dict['ID'] = type_ID
                    add_type_dict['Name'] = type_name
                    add_type_dict['Atk_Effectiveness'] = type_atkeffec_list
                    add_type_dict['Description'] = type_desc

                    self.typechart[type_ID] = add_type_dict
                    line_count += 1

    # Abilites Functions
    # get all possible abilities
    def get_abilities(self): 
        return self.abilities.keys()

    # get specific ability information
    def get_ability(self, ability_ID):
        try:
            ability = self.abilities[ability_ID] 
        except Exception:
            ability = None
        return ability

    # Items Functions
    # get all possible items
    def get_items(self):
        return self.items.keys()

    # get specific item information
    def get_item(self, item_ID):
        try:
            item = self.items[item_ID]
        except Exception:
            item = None
        return item

    # Moves Functions
    # get all possible moves
    def get_moves(self):
        return self.moves.keys()

    # get specific move information
    def get_move(self, move_ID):
        try:
            move = self.moves[move_ID]
        except Exception:
            move = None
        return move

    # Nature Functions
    # get all possible natures
    def get_natures(self):
        return self.natures.keys()

    # get specific nature
    def get_nature(self, nature_ID):
        try:
            nature = self.natures[nature_ID]
        except Exception:
            nature = None
        return nature

    # Pokemon Functions
    # get all pokemon in pokedex
    def get_pokemon_all(self):
        return self.pokemon.keys()

    # get specific info from pokemon nat dex number
    def get_pokemon_one(self, dex_ID):
        try:
            pokemon = self.pokemon[dex_ID]
        except Exception:
            pokemon = None
        return pokemon
    
    def get_pokemon_form(self, dex_ID, form_name):
        try:
            pokemon = self.pokemon[dex_ID][form_name]
        except Exception:
            pokemon = None
        return pokemon

    # Type Chart Functions
    # get all type multipliers
    def get_typecharts(self):
        return self.typechart.keys()

    # get specific
    def get_typechart(self, type_ID):
        try:
            type_chart = self.typechart[type_ID]
        except Exception:
            type_chart = None
        return type_chart
  
    # Party Pokemon Functions
    # get all party members information
    def get_party_members(self):
        if self.party_members:
            return self.party_members

    # get specific member information
    def get_party_member(self,slot_ID):
        slot_ID = str(slot_ID)
        try:
            party_member = self.party_members[slot_ID]
        except Exception:
            party_member = None
        return party_member

    # add new member
    # given nat dex num, form name, nature, ability, held item, 4 moves
    def set_party_member(self, slot_ID, dex_ID, form_name, gender, nature_ID, ability_ID, item_ID, move1_ID, move2_ID, move3_ID, move4_ID):
        
        slot_ID = str(slot_ID)
        dex_ID = int(dex_ID)
        form_name = str(form_name)
        gender = str(gender).title()
        nature_ID = int(nature_ID)
        ability_ID = int(ability_ID)
        item_ID = int(item_ID)
        move1_ID = int(move1_ID)
        move2_ID = int(move2_ID)
        move3_ID = int(move3_ID)
        move4_ID = int(move4_ID)
        move_list = [move1_ID,move2_ID,move3_ID,move4_ID]
        
        new_pokemon = self.get_pokemon_form(dex_ID,form_name)
        
        if new_pokemon is None:
            #pokemon does not exist
            return False

        learnable_moves = new_pokemon['Info']['Learnable Moves']

        if len(move_list) != len(set(move_list)):
            #Duplicate move entered
            return False

        for move_ID in move_list:
            temp_move = self.get_move(move_ID)
            if temp_move['Name'] not in learnable_moves:
                #Not a learnable move in moveset
                return False
        
        possible_abilities = new_pokemon['Info']['Abilities']

        ability_dict = self.get_ability(ability_ID)
        ability_name = ability_dict['Name']
        
        if ability_name not in possible_abilities:
            #Not a valid ability
            return False

        add_pokemon = dict()
        add_pokemon['Dex_Number'] = dex_ID
        add_pokemon['Name'] = form_name

        #default to male, change if valid gender entered
        add_pokemon['Gender'] = 'Male'

        if gender == 'Male' or gender == 'Female' or gender == 'None':
            add_pokemon['Gender'] = gender

        add_pokemon['Typing'] =  new_pokemon['Info']['Types']

        nature_dict = self.get_nature(nature_ID)
        nature_name = nature_dict['Name']
        add_pokemon['Nature'] = nature_name
        
        add_pokemon['Ability'] = ability_name

        item_dict = self.get_item(item_ID)
        item_name = item_dict['Name'] 
        add_pokemon['Held_Item'] = item_name

        
        move_name_list = list()
        for move_id in move_list:
            move_dict = self.get_move(move_id)
            move_name = move_dict['Name']
            move_name_list.append(move_name)

        add_pokemon['Move_1'] = move_name_list[0] 
        add_pokemon['Move_2'] = move_name_list[1]
        add_pokemon['Move_3'] = move_name_list[2]
        add_pokemon['Move_4'] = move_name_list[3]

        self.party_members[slot_ID] = add_pokemon
        return True
        
    # get member's moves
    def get_party_member_moves(self,slot_id):
        slot_id = str(slot_id)
        if self.party_members[slot_id]:
            move_list = list()
            move1 = self.party_members[slot_id]['Move_1']
            move2 = self.party_members[slot_id]['Move_2']
            move3 = self.party_members[slot_id]['Move_3']
            move4 = self.party_members[slot_id]['Move_4']
            move_list.append(move1)
            move_list.append(move2)
            move_list.append(move3)
            move_list.append(move4)
            return move_list 

    # change member's moves
    def set_party_member_move(self, slot_id, move_slot, new_move_ID):
        slot_id = str(slot_id)
        if self.party_members[slot_id]:
            if int(move_slot) > 0 & int(move_slot) <= 4:
                curr_move1 = self.party_members[slot_id]['Move_1']
                curr_move2 = self.party_members[slot_id]['Move_2']
                curr_move3 = self.party_members[slot_id]['Move_3']
                curr_move4 = self.party_members[slot_id]['Move_4']

                curr_move_list = list()
                curr_move_list.append(curr_move1)
                curr_move_list.append(curr_move2)
                curr_move_list.append(curr_move3)
                curr_move_list.append(curr_move4)

                dex_ID = self.party_members[slot_id]['Dex_Number']
                form_name = self.party_members[slot_id]['Name']
                pokemon = self.get_pokemon_form(dex_ID,form_name)
                learnable_moves = pokemon['Info']['Learnable_Moves']

                move_dict = self.get_move(new_move_ID)
                new_move_name = move_dict['Name']

                if new_move_name not in curr_move_list:
                    valid_move = any(new_move_name in move for move in learnable_moves)
                    if valid_move == False:
                        return False
                    else:
                        if move_slot == 1:
                            self.party_members[slot_id]['Move_1'] = new_move_name
                        if move_slot == 2:
                            self.party_members[slot_id]['Move_2'] = new_move_name
                        if move_slot == 3:
                            self.party_members[slot_id]['Move_3'] = new_move_name
                        if move_slot == 4:
                            self.party_members[slot_id]['Move_4'] = new_move_name
                        return True
    
    def set_party_member_gender(self,slot_id,new_gender):
        slot_id = str(slot_id)
        if self.party_members[slot_id]:

            if gender == 'Male' or gender == 'Female' or gender == 'None':
                self.party_members[slot_id]['Gender'] = new_gender
                return True
            else:
                return False

    # change member's nature
    def set_party_member_nature(self,slot_id,new_nature_id):
        slot_id = str(slot_id)
        if self.party_members[slot_id]:
            possible_nature_ids = self.get_natures()
            possible_nature_ids = possible_nature_ids
            if new_nature_id in possible_nature_ids:
                new_nature = self.get_nature(new_nature_id)
                new_nature_name = new_nature['Name']
                self.party_members[slot_id]['Nature'] = new_nature_name
                return True
            else:
                return False
            # given nature and slot number, change nature
    
    # change member's ability
    def set_party_member_ability(self, slot_id, new_ability_ID):
        slot_id = str(slot_id)
        if self.party_members[slot_id]:
            dex_ID = self.party_members[slot_id]['Dex_Number']
            form_name = self.party_members[slot_id]['Name']
            pokemon = self.get_pokemon_form(dex_ID,form_name)
            possible_abilities = pokemon['Info']['Abilities']

            new_ability = self.get_ability(new_ability_ID)
            new_ability_name = new_ability['Name']
            
            if new_ability_name in possible_abilities:
                self.party_members[slot_id]['Ability'] = new_ability_name
                return True
            else:
                return False
        # given slot number, change ability to applicable ability
    
    # change member's held item
    def set_party_member_item(self, slot_id, new_item_id):
        slot_id = str(slot_id)
        if self.party_members[slot_id]:
            possible_item_ids = self.get_items()
            if new_item_id in possible_item_ids:
                new_item = self.get_item(new_item_id)
                new_item_name = new_item['Name']
                self.party_members[slot_id]['Held_Item'] = new_item_name
                return True
            else:
                return False
            # given slot number, change held item
    
    # remove all party members
    def delete_party(self):
        if self.party_members:
            del self.party_members
            self.party_members = dict()
            self.party_members['1'] = None
            self.party_members['2'] = None
            self.party_members['3'] = None
            self.party_members['4'] = None
            self.party_members['5'] = None
            self.party_members['6'] = None
    
    # remove one party member
    def delete_party_member(self,slot_id):
        slot_id = str(slot_id)
        if self.party_members[slot_id]:
            del self.party_members[slot_id]
            self.party_members[slot_id] = None


if __name__ == "__main__":
    pdb = _pokemon_database()
    
    pdb.load_abilities('abilities.csv')
    pdb.load_items('items.csv')
    pdb.load_moves('moves.csv')
    pdb.load_natures('natures.csv')
    pdb.load_pokemon('pokemonandmoves.csv')
    pdb.load_typechart('types.csv')

    #GET FUNCTIONS ALL WORK FOR CSV DATA

    slot_ID = 1
    dex_ID = 1
    form_name = 'Bulbasaur'
    gender = 'Male'
    nature_ID = 2
    ability_ID = 46
    item_ID = 3
    move1_ID = 3
    move2_ID = 224
    move3_ID = 7
    move4_ID = 253

    pdb.set_party_member(slot_ID, dex_ID, form_name, gender, nature_ID, ability_ID, item_ID, move1_ID, move2_ID, move3_ID, move4_ID)
    print(pdb.get_pokemon_all())

    